package by.beerlabel.admin.controller;

import by.beerlabel.common.dao.impl.BeerDAO;
import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.dto.UserTO;
import by.beerlabel.common.dto.vo.Beer;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BeerManage;
import by.beerlabel.common.service.impl.BeerManageImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.json.Json;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dima on 11.5.15.
 */
@Controller
@RequestMapping("/beers")
public class BeerController {

    @Autowired
    private BeerManage beerManage;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String beer() throws TechnicalException, ServiceException {




        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<Beer> beers = beerManage.getAll();

        for (Beer beer : beers) {

            JSONObject beerObject = new JSONObject();
            beerObject.put("id", beer.getBeerTO().getId());
            beerObject.put("name", beer.getBeerTO().getName());
            beerObject.put("brewery", beer.getBrewery().getBreweryTO().getName());
            beerObject.put("sort", beer.getSort().getName());
            beerObject.put("image", beer.getBeerTO().getImgLink());
            beerObject.put("description", beer.getBeerTO().getDescription());
            beerObject.put("alc", beer.getBeerTO().getAlc());

            array.add(beerObject);

        }


        data.put("beers", array);

        return data.toJSONString();
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String add(@RequestParam String name,
                      @RequestParam String image,
                      @RequestParam Long brewery,
                      @RequestParam Long sort,
                      @RequestParam double alc,
                      @RequestParam String description,
                      @RequestParam List<Long> tags) throws TechnicalException, ServiceException {

        BeerTO beerTO = new BeerTO();
        beerTO.setName(name);
        beerTO.setBreweryId(brewery);
        beerTO.setSortId(sort);
        beerTO.setAlc(alc);
        beerTO.setImgLink(image);
        beerTO.setDescription(description);

        Beer beer = new Beer();
        beer.setBeerTO(beerTO);

        List<TagTO> listTags = new LinkedList<>();

        for (Long tag : tags) {

            TagTO tagTO = new TagTO();
            tagTO.setId(tag);

            listTags.add(tagTO);

        }

        beer.setTags(listTags);

        beerManage.save(beer);

        return "";

    }
}
