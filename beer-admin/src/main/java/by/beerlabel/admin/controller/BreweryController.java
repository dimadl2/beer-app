package by.beerlabel.admin.controller;

import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.CountryTO;
import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.dto.vo.Brewery;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BreweryManage;
import by.beerlabel.common.service.BreweryService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Controller
@RequestMapping("/brewery")
public class BreweryController {

    @Autowired
    private BreweryManage breweryManage;

    @Autowired
    private BreweryService breweryService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sorts() throws TechnicalException, ServiceException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

       List<Brewery> breweries = breweryManage.getAll();

        for (Brewery brewery : breweries) {

            JSONObject breweryObject = new JSONObject();

            breweryObject.put("name", brewery.getBreweryTO().getName());
            breweryObject.put("id", brewery.getBreweryTO().getId());
            breweryObject.put("country", brewery.getCountry().getName());

            array.add(breweryObject);

        }


        data.put("breweries", array);

        return data.toJSONString();
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String add(@RequestParam String brewery,
                      @RequestParam Long country) throws TechnicalException, ServiceException {

        BreweryTO breweryTO = new BreweryTO();
        breweryTO.setName(brewery);
        breweryTO.setCountryId(country);

        breweryService.save(breweryTO);

        return "";

    }

    @RequestMapping(value = "/get/{id}",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String get(@PathVariable("id") Long id) throws TechnicalException, ServiceException {

        BreweryTO breweryTO = breweryService.fetchById(id);

        JSONObject breweryObject = new JSONObject();
        breweryObject.put("name", breweryTO.getName());
        breweryObject.put("id", breweryTO.getId());
        breweryObject.put("country", breweryTO.getCountryId());

        return breweryObject.toJSONString();
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String update(@RequestParam String brewery,
                         @RequestParam Long id,
                         @RequestParam Long country) throws TechnicalException {

        BreweryTO breweryTO = new BreweryTO();
        breweryTO.setCountryId(country);
        breweryTO.setId(id);
        breweryTO.setName(brewery);

        breweryService.update(breweryTO);

        return "";

    }

}
