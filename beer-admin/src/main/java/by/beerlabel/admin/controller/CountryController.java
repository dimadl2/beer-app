package by.beerlabel.admin.controller;

import by.beerlabel.common.dto.CountryTO;
import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.dto.vo.Beer;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.CountryService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Controller
@RequestMapping("/country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String countries() throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<CountryTO> countryTOs = countryService.getAll();

        for (CountryTO country : countryTOs) {

            JSONObject countryObject = new JSONObject();
            countryObject.put("name", country.getName());
            countryObject.put("id", country.getId());
            countryObject.put("abbr", country.getAbbr());

            array.add(countryObject);

        }

        data.put("countries", array);

        return data.toJSONString();
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String add(@RequestParam String country,
                      @RequestParam String abbr) throws TechnicalException, ServiceException {

        CountryTO countryTO = new CountryTO(country, abbr);
        countryService.save(countryTO);

        return "";
    }
    @RequestMapping(value="/get/{id}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String get(@PathVariable("id") Long id) throws TechnicalException, ServiceException {

        CountryTO country = countryService.fetchById(id);

            JSONObject countryObject = new JSONObject();
            countryObject.put("name", country.getName());
            countryObject.put("id", country.getId());
            countryObject.put("abbr", country.getAbbr());


        return countryObject.toJSONString();
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String update(@RequestParam String country,
                      @RequestParam String abbr,
                      @RequestParam Long id) throws TechnicalException {

        CountryTO countryTO = new CountryTO(country, abbr);
        countryTO.setId(id);
        countryService.update(countryTO);

        return "";
    }

}
