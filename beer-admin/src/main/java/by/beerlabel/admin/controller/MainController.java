package by.beerlabel.admin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.enterprise.inject.Model;

/**
 * Created by dima on 15.5.15.
 */
@Controller
public class MainController {

    @RequestMapping("/main")
    public ModelAndView load(){

        ModelAndView model = new ModelAndView();

        model.setViewName("main");

        return model;

    }

}
