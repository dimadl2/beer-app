package by.beerlabel.admin.controller;

import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.dto.vo.Beer;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.SortService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Controller
@RequestMapping(value = "/sorts")
public class SortController {

    @Autowired
    private SortService sortService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sorts() throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<SortTO> sorts = sortService.getAll();

        for (SortTO sort: sorts) {

            JSONObject sortObject = new JSONObject();
            sortObject.put("name", sort.getName());
            sortObject.put("id", sort.getId());

            array.add(sortObject);

        }


        data.put("sorts", array);

        return data.toJSONString();
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String add(@RequestParam String sort) throws TechnicalException, ServiceException {

        SortTO sortTO = new SortTO(sort);

        sortService.save(sortTO);

        return "";

    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String delete(@RequestParam List<Long> sortId) throws TechnicalException {

        sortService.delete(sortId);
        return "";

    }

    @RequestMapping(value = "/get/{id}",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String get(@PathVariable("id") Long id) throws TechnicalException, ServiceException {


        SortTO sort = sortService.fetchById(id);

            JSONObject sortObject = new JSONObject();
            sortObject.put("name", sort.getName());
            sortObject.put("id", sort.getId());

        return sortObject.toJSONString();
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String update(@RequestParam String sort,
                         @RequestParam Long id) throws TechnicalException {

        SortTO sortTO = new SortTO(sort);
        sortTO.setId(id);

        sortService.update(sortTO);

        return "";

    }

    @RequestMapping(value="/country/{id}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sortsByCountry(@PathVariable Long id) throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<SortTO> sorts = sortService.fetchSortsByCountry(id);

        for (SortTO sort: sorts) {

            JSONObject sortObject = new JSONObject();
            sortObject.put("name", sort.getName());
            sortObject.put("id", sort.getId());

            array.add(sortObject);

        }


        data.put("sorts", array);

        return data.toJSONString();
    }

    @RequestMapping(value="/brewery/{id}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sortsByBrewery(@PathVariable Long id) throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<SortTO> sorts = sortService.fetchSortsByBrewery(id);

        for (SortTO sort: sorts) {

            JSONObject sortObject = new JSONObject();
            sortObject.put("name", sort.getName());
            sortObject.put("id", sort.getId());

            array.add(sortObject);

        }


        data.put("sorts", array);

        return data.toJSONString();
    }
}
