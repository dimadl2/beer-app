package by.beerlabel.admin.controller;

import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.TagService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Controller
@RequestMapping("/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String tags() throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<TagTO> tags = tagService.getAll();

        for (TagTO tag : tags) {

            JSONObject tagObject = new JSONObject();
            tagObject.put("name", tag.getName());
            tagObject.put("id", tag.getId());

            array.add(tagObject);

        }


        data.put("tags", array);

        return data.toJSONString();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String add(@RequestParam String tag) throws TechnicalException, ServiceException {

        TagTO tagTO = new TagTO(tag);

        tagService.save(tagTO);

        return "";

    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String get(@PathVariable("id") Long id) throws TechnicalException, ServiceException {


        TagTO tag = tagService.fetchById(id);

        JSONObject tagObject = new JSONObject();
        tagObject.put("name", tag.getName());
        tagObject.put("id", tag.getId());

        return tagObject.toJSONString();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String update(@RequestParam String tag,
                         @RequestParam Long id) throws TechnicalException {

        TagTO tagTO = new TagTO(tag);
        tagTO.setId(id);

        tagService.update(tagTO);

        return "";

    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String delete(@RequestParam List<Long> tagId) throws TechnicalException {

        tagService.delete(tagId);
        return "";

    }

    @RequestMapping(value = "/beer/{id}",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String tagsByBeer(@PathVariable Long id) throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<TagTO> tags = tagService.fetchTagsByBeerId(id);

        for (TagTO tag : tags) {

            JSONObject tagObject = new JSONObject();
            tagObject.put("name", tag.getName());
            tagObject.put("id", tag.getId());

            array.add(tagObject);

        }


        data.put("tags", array);

        return data.toJSONString();
    }
}
