package by.beerlabel.admin.controller;

import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.dto.UserTO;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.UserService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * Created by dima on 26.4.15.
 */
@Controller
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getAll() throws TechnicalException {

        List<UserTO> users = userService.getAll();

        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        for (UserTO user : users) {

            JSONObject userJson = new JSONObject();
            userJson.put("username", user.getUsername());
            userJson.put("email", user.getEmail());
            userJson.put("birth", user.getDateBirth());
            userJson.put("sex", user.getSex());
            userJson.put("id", user.getId());

            array.add(userJson);
        }

        data.put("users", array);

        return data.toJSONString();
    }

}
