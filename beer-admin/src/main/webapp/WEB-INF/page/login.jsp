<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
<head>
  <title>Log In</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/login.css'/>">
</head>

<body>
<div class="logo"><img src="<c:url value='/img/logo.png'/>"></div>
<div class="text" align="center">Log In</div>

<div class="login" align="center">
  <form name="loginForm" action="<c:url value='/j_spring_security_check' />" method="POST">
    <input type="text" name="j_username" placeholder="<spring:message code="label.username"/>"></br>
    <input type="password" name="j_password" placeholder="<spring:message code="label.password"/>"></br>
    <input type="submit" value="Log In" style="width: 100px;" class="button1"></br>

  </form>
</div>

</body>



</html>
