<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
  <script type="text/javascript" src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/main.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/forms.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/js/edit-forms.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/choosen/chosen.jquery.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/choosen/docsupport/prism.js'/>"></script>
  <link href="<c:url value='/css/main.css'/>" rel="stylesheet" type="text/css">
  <link href="<c:url value='/choosen/docsupport/style.css'/>" rel="stylesheet" type="text/css">
  <link href="<c:url value='/choosen/docsupport/prism.css'/>" rel="stylesheet" type="text/css">
  <link href="<c:url value='/choosen/chosen.css'/>" rel="stylesheet" type="text/css">
  <link href="<c:url value='/css/downloader.css'/>" rel="stylesheet" type="text/css">
</head>
<body>
<div class="tabs">
  <ul class="ul-tabs">
    <li>Users</li>
    <li>Beers</li>
    <li>Sorts</li>
    <li>Countries</li>
    <li>Breweries</li>
    <li>Tags</li>

  </ul>
  <div id="form">
    <div id="add-sort" style="display: none">
      <form id="form-sort-add" method="post">
        New sort: <input type="text" id="sort-name" name="sort">
        <input type="submit" value="Save">
        <input type="button" value="Cancel" onclick="$('#add-sort').css({'display':'none'})">
      </form>
    </div>
    <div id="add-country" style="display:none">
      <form id="form-country-add" method="post">
        <table>
          <tr>
            <td>Country name:</td>
            <td><input type="text" id="country-name" name="country"></td>
          </tr>
          <tr>
            <td> Abbreviation:</td>
            <td><input type="text" name="abbr" id="abbr"></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <input type="submit" value="Save">
              <input type="button" value="Cancel" onclick="$('#add-country').css({'display':'none'})">
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div id="add-tag" style="display:none">
      <form id="form-tag-add" method="post">
        New tag: <input type="text" id="tag-name" name="tag">
        <input type="submit" value="Save">
        <input type="button" value="Cancel" onclick="$('#add-tag').css({'display':'none'})">
      </form>
    </div>
    <div id="add-brewery" style="display:none">
      <form id="form-brewery-add" method="post">
        <table>
          <tr>
            <td>Brewery name:</td>
            <td><input type="text" id="brewery-name" name="brewery"></td>
          </tr>
          <tr>
            <td>Country:</td>
            <td><select id="l-country" name="country"></select></td>
          </tr>
          <tr>
            <td colspan="2" align="right">
              <input type="submit" value="Save">
              <input type="button" value="Cancel" onclick="$('#add-brewery').css({'display':'none'})">
            </td>
          </tr>
        </table>
      </form>
    </div>
    <div id="add-beer" style="display:none">
      <form id="form-beer-add" method="post" style="margin: 0 auto">
        <table style="margin: 0 auto;">
          <tr>
            <td>Beer name: </td>
            <td><input type="text" id="beer-name" name="name"></td>
          </tr>
          <tr>
            <td>Brewery:</td>
            <td><select id="l-brewery" name="brewery"></select></td>
          </tr>
          <tr>
            <td>Sort:</td>
            <td><select id="l-sort" name="sort"></select></td>
          </tr>
          <tr>
            <td>ALC:</td>
            <td><input type="text" name="alc"></td>
          </tr>
          <tr>
            <td>Image:</td>
            <td><input type="text" name="image"></td>
          </tr>
          <tr>
            <td valign="top">Description: </td>
            <td valign="top"><textarea rows="5" cols="37" name="description"></textarea></td>
          </tr>
          <tr>
            <td>Tags:</td>
            <td><select id="l-tags" name="tags" data-placeholder="Choose tags..." style="width:350px;" multiple class="chosen-select"></select></td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <div style="margin-top: 20px">
              <input type="submit" value="Save">
              <input type="button" value="Cancel" onclick="$('#add-beer').css({'display':'none'})">
              </div>
            </td>

          </tr>
        </table>
      </form>
    </div>
  </div>
  <div id="container">
    <div>
      <div id="users"></div>
    </div>
    <div>
      <div id="beers"></div>
    </div>
    <div></div>
    <div>Countries</div>
    <div>Breweries</div>
    <div>Tags</div>
  </div>
</div>

<div class="over">

</div>
<div class="dialog">

  <form id="form-country-edit" class="hide">
    <table>
      <tr>
        <td>Country name:</td>
        <td><input type="text" name="country" id="country-name-edit"></td>
      </tr>
      <tr>
        <td>Abbreviation: </td>
        <td><input type="text" name="abbr" id="abbr-edit"></td>
      </tr>
      <tr>
        <td colspan="2" align="right">
          <input type="hidden" name="id" id="country-id">
          <input type="submit" value="Save">
          <input class="cancel" type="button" value="Cancel">
        </td>
      </tr>
    </table>
  </form>

  <form id="form-sort-edit" class="hide">
    Sort name: <input type="text" name="sort" id="sort-name-edit">
    <input type="hidden" name="id" id="sort-id">
    <input type="submit" value="Save">
    <input class="cancel" type="button" value="Cancel">
  </form>

  <form id="form-tag-edit" class="hide">
    Tag name: <input type="text" name="tag" id="tag-name-edit">
    <input type="hidden" name="id" id="tag-id">
    <input type="submit" value="Save">
    <input class="cancel" type="button" value="Cancel">
  </form>

  <form id="form-brewery-edit" class="hide">
    <table>
      <tr>
        <td>Brewery name:</td>
        <td><input type="text" name="brewery" id="brewery-name-edit"></td>
      </tr>
      <tr>
        <td>Country:</td>
        <td><select id="l-country-edit" name="country"></select></td>
      </tr>
      <tr>
        <td colspan="2" align="right">
          <input type="hidden" name="id" id="brewery-id">
          <input type="submit" value="Save">
          <input class="cancel" type="button" value="Cancel">
        </td>
      </tr>
    </table>
  </form>


</div>

</body>
</html>
