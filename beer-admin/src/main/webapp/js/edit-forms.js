/**
 * Created by dima on 16.5.15.
 */

$(document).ready(function(){

    $(".cancel").click(function(){

        $(".over").css({"display":"none"});
        $(".dialog").css({"display": "none"});
        $(".dialog form").css({"display": "none"});

    })

})

function openEditForm(i, id){

    $(".over").css({"display":"block"});
    $(".dialog").css({"display": "block"});

    if (i === 1){



    }else if(i === 2) {

        $("#form-sort-edit").css({"display": "block"});

        var url = '/beer-admin/sorts/get/'+id;

        $.ajax({
            type: 'GET',
            url: url,
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                $("#sort-name-edit").val(result["name"]);
                $("#sort-id").val(result["id"]);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

    } else if(i === 3){

        $("#form-country-edit").css({"display": "block"});

        var url = '/beer-admin/country/get/'+id;

        $.ajax({
            type: 'GET',
            url: url,
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                $("#country-name-edit").val(result["name"]);
                $("#abbr-edit").val(result["abbr"]);
                $("#country-id").val(result["id"]);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

    }else if (i === 4){

        $("#form-brewery-edit").css({"display": "block"});

        function load() {

            var url = '/beer-admin/brewery/get/' + id;

            $.ajax({
                type: 'GET',
                url: url,
                cache: false,
                dataType: 'json',
                async: true,
                success: function (result) {

                    $("#brewery-name-edit").val(result["name"]);
                    $("#l-country-edit").children("option").each(function(){
                        if ($(this).val() == result["country"]){
                            $(this).attr('selected', 'selected');
                        }
                    });
                    $("#brewery-id").val(result["id"]);

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.status + ' ' + jqXHR.responseText);
                }
            });
        }

        $.ajax({
            type: 'GET',
            url: '/beer-admin/country',
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                var str = "";

                for (i= 0; i < result["countries"].length; i++){

                    str += "<option value='"+result["countries"][i]["id"]+"'>"+ result["countries"][i]["name"] +"</option>"

                }
                $("#l-country-edit").html(str);
                load();

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });





    }else if (i === 5){

        $("#form-tag-edit").css({"display": "block"});

        var url = '/beer-admin/tag/get/'+id;

        $.ajax({
            type: 'GET',
            url: url,
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                $("#tag-name-edit").val(result["name"]);
                $("#tag-id").val(result["id"]);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

    }

}

