/**
 * Created by dima on 15.5.15.
 */
$(document).ready(function(){



    $("#form-sort-add").submit(function(){
        var msg = $(this).serialize();

        var sortName = $("#sort-name").val();
        sortName = $.trim(sortName);
        if (sortName === ""){
            alert("Sort name is empty! Enter the sort name.")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/sorts/add',
            data: msg,
            success: function(){
                showPage(2);
                return false;
            },
            error: function(){
                showPage(2);
                return false;
            }
        });

        return false;
    });

    $("#form-beer-add").submit(function(){
        var msg = $(this).serialize();

        $.ajax({

            type: 'POST',
            url: '/beer-admin/beers/add',
            data: msg,
            success: function(){
                showPage(1);
                return false;
            },
            error: function(){
                showPage(1);
                return false;
            }
        });

        return false;
    });

    $("#form-country-add").submit(function(){
        var msg = $(this).serialize();

        var sortName = $("#country-name").val();
        var abbr = $("#abbr").val();
        abbr = $.trim(abbr);
        sortName = $.trim(sortName);
        if (sortName === "" || abbr === ""){
            alert("Fill all fields!")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/country/add',
            data: msg,
            success: function(){
                showPage(3);
            },
            error: function(){
                showPage(3);
            }
        });

        return false;
    });

    $("#form-country-edit").submit(function(){
        var msg = $(this).serialize();

        var sortName = $("#country-name-edit").val();
        var abbr = $("#abbr-edit").val();
        abbr = $.trim(abbr);
        sortName = $.trim(sortName);
        if (sortName === "" || abbr === ""){
            alert("Fill all fields!")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/country/update',
            dataType: 'json',
            data: msg,
            success: function(){
                closeEditForm()
                showPage(3);
            },
            error: function(){
                closeEditForm()
                showPage(3);
            }
        });

        return false;
    });

    $("#form-brewery-edit").submit(function(){
        var msg = $(this).serialize();

        var sortName = $("#brewery-name-edit").val();
        sortName = $.trim(sortName);
        if (sortName === ""){
            alert("Fill all fields!")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/brewery/update',
            dataType: 'json',
            data: msg,
            success: function(){
                closeEditForm()
                showPage(4);
            },
            error: function(){
                closeEditForm()
                showPage(4);
            }
        });

        return false;
    });

    $("#form-sort-edit").submit(function(){
        var msg = $(this).serialize();

        var sortName = $("#sort-name-edit").val();
        sortName = $.trim(sortName);
        if (sortName === ""){
            alert("Sort name is empty! Enter the sort name.")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/sorts/update',
            data: msg,
            success: function(){
                closeEditForm()
                showPage(2);
            },
            error: function(){
                closeEditForm();
                showPage(2);
            }
        });

        return false;
    });

    $("#form-tag-add").submit(function(){
        var msg = $(this).serialize();

        var sortName = $("#tag-name").val();
        sortName = $.trim(sortName);
        if (sortName === ""){
            alert("Tag name is empty! Enter the tag name.")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/tag/add',
            data: msg,
            success: function(){
                showPage(5);
            },
            error: function(){
                showPage(5);
            }
        });

        return false;
    });

    $("#form-tag-edit").submit(function(){
        var msg = $(this).serialize();

        var tagName = $("#tag-name-edit").val();
        tagName = $.trim(tagName);
        if (tagName === ""){
            alert("Tag name is empty! Enter the tag name.")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/tag/update',
            data: msg,
            success: function(){
                closeEditForm()
                showPage(5);
            },
            error: function(){
                closeEditForm();
                showPage(5);
            }
        });

        return false;
    });

    $("#form-brewery-add").submit(function(){
        var msg = $(this).serialize();

        var breweryName = $("#brewery-name").val();
        breweryName = $.trim(breweryName);
        if (breweryName === ""){
            alert("Brewery name is empty! Enter the brewery name.")
            return false;
        }

        $.ajax({

            type: 'POST',
            url: '/beer-admin/brewery/add',
            data: msg,
            success: function(){
                showPage(4);
            },
            error: function(){
                showPage(4);
            }
        });

        return false;
    });


});
function deleteSorts () {
    if (confirm("Are you sure?")) {
        var msg = $("#delete-sorts").serialize();
        $.ajax({

            type: 'POST',
            url: '/beer-admin/sorts/delete',
            data: msg,
            success: function () {
                showPage(2);
                return false;
            },
            error: function () {
                showPage(2);
                return false;
            }
        });
        return false;
    }else{
        return false;
    }


}

function deleteTags () {
    if (confirm("Are you sure?")) {
        var msg = $("#delete-tags").serialize();
        $.ajax({

            type: 'POST',
            url: '/beer-admin/tag/delete',
            data: msg,
            success: function () {
                showPage(5);
            },
            error: function () {
                showPage(5);
            }
        });
        return false;
    }else{
        return false;
    }


}
