/**
 * Created by dima on 26.4.15.
 */
(function($){
    jQuery.fn.lightTabs = function(options){

        var createTabs = function(){
            tabs = this;
            i = 0;

            getTableBeer = function(result){

                var str = "<form method='post' id='delete-beer' onsubmit='return deleteBeers();'>" +
                    "<div align='right'>" +
                    "<input type='button' value='Add' onclick='openForm(\"add-beer\")'>" +
                    "<input type='submit' value='Delete'>" +
                    "</div>" +
                    "<table class='table'>";

                for (i=0; i<result["beers"].length;i++) {

                        str +=
                        "<tr >" +
                        "<td style='vertical-align: middle;'>" + result['beers'][i]['id']+"</td>" +
                        "<td style='vertical-align: middle;'><img src='"+ result['beers'][i]['image']+"'/></td>" +
                        "<td style='vertical-align: middle;'>" + result['beers'][i]['name']+"</td>" +
                        "<td style='vertical-align: middle;'>"+ result['beers'][i]['brewery']+"</td>" +
                        "<td style='vertical-align: middle;'>"+ result['beers'][i]['sort']+"</td>" +
                        "<td style='vertical-align: middle;'>"+ result['beers'][i]['alc']+"</td>" +
                        "<td style='vertical-align: middle; width: 400px'>"+ result['beers'][i]['description']+"</td>" +

                        "<td style='vertical-align: middle;'><input type='checkbox' name='beerId' value='"+result['beers'][i]['id']+"'></td>" +
                        "</tr>"
                        ;
                }

                str +="</table>";

                return str;
            }

            getTableSort = function(result){

                var str =
                    "<form method='post' id='delete-sorts' onsubmit='return deleteSorts();'>" +
                    "<div align='right'>" +
                    "<input type='button' value='Add' onclick='openForm(\"add-sort\")'>" +
                    "<input type='submit' value='Delete'>" +
                    "</div>" +
                    "<table class='table'>";

                for (i=0; i<result["sorts"].length;i++) {

                    var id = result['sorts'][i]['id'];

                    str +=
                        "<tr ondblclick='openEditForm(2,"+id+")'>" +
                        "<td>" + result['sorts'][i]['id']+"</td>" +
                        "<td>"+ result['sorts'][i]['name']+"</td>" +
                        "<td><input type='checkbox' name='sortId' value='"+result['sorts'][i]['id']+"'></td>" +
                        "</tr>"
                    ;
                }

                str +="</table></form>";

                return str;
            }

            getTableUser = function(result){



                var str = "<table class='table'>";;

                for (i=0; i<result["users"].length;i++) {

                    var id = result['users'][i]['id'];
                    str +=
                        "<tr>" +
                        "<td>" + result['users'][i]['id']+"</td>" +
                        "<td>"+ result['users'][i]['username']+"</td>" +
                        "<td>"+ result['users'][i]['sex']+"</td>" +
                        "<td>"+ result['users'][i]['birth']+"</td>" +
                        "<td>"+ result['users'][i]['email']+"</td>" +
                        "</tr>";
                }

                str +="</table>";

                return str;
            }

            getTableTag = function(result){



                var str = "<form method='post' id='delete-tags' onsubmit='return deleteTags();'>" +
                    "<div align='right'>" +
                    "<input type='button' value='Add' onclick='openForm(\"add-tag\")'>" +
                    "<input type='submit' value='Delete'>" +
                    "</div>" +
                    "<table class='table'>";;

                for (i=0; i<result["tags"].length;i++) {

                    var id = result['tags'][i]['id'];
                    str +=
                        "<tr ondblclick='openEditForm(5,"+id+")'>" +
                        "<td>" + result['tags'][i]['id']+"</td>" +
                        "<td>"+ result['tags'][i]['name']+"</td>" +
                        "<td><input type='checkbox' name='tagId' value='"+result['tags'][i]['id']+"'></td>" +
                        "</tr>"
                    ;
                }

                str +="</table>";

                return str;
            }

            getTableCountry = function(result){

                var str =
                    "<form method='post' id='delete-country' onsubmit='return deleteCountry();'>" +
                    "<div align='right'>" +
                    "<input type='button' value='Add' onclick='openForm(\"add-country\")'>" +
                    "<input type='submit' value='Delete'>" +
                    "</div>"+
                    "<table class='table'>";

                for (i=0; i<result["countries"].length;i++) {

                    var id = result['countries'][i]['id'];

                    str +=
                        "<tr ondblclick='openEditForm(3,"+id+")'>" +
                        "<td>" + result['countries'][i]['id']+"</td>" +
                        "<td>"+ result['countries'][i]['name']+"</td>" +
                        "<td>"+ result['countries'][i]['abbr']+"</td>" +
                        "<td><input type='checkbox' name='countryId' value='"+result['countries'][i]['id']+"'></td>" +
                        "</tr>"
                    ;
                }

                str +="</table></form>";

                return str;
            }

            getTableBrewery = function(result){

                var str = "<form method='post' id='delete-brewery' onsubmit='return deleteCountry();'>" +
                    "<div align='right'>" +
                    "<input type='button' value='Add' onclick='openForm(\"add-brewery\")'>" +
                    "<input type='submit' value='Delete'>" +
                    "</div>" +
                    "<table class='table'>";

                for (i=0; i<result["breweries"].length;i++) {

                    var id = result['breweries'][i]['id'];

                    str +=
                        "<tr ondblclick='openEditForm(4,"+id+")'>" +
                        "<td>" + result['breweries'][i]['id']+"</td>" +
                        "<td>"+ result['breweries'][i]['name']+"</td>" +
                        "<td>"+ result['breweries'][i]['country']+"</td>" +
                        "<td><input type='checkbox' name='breweryId' value='"+result['breweries'][i]['id']+"'></td>" +
                        "</tr>"
                    ;
                }

                str +="</table>";

                return str;
            }

            loadData = function(i){

                $(tabs).children("div#container").children("div").eq(i).html("<div id='circularG'>"
                +"<div id='circularG_1' class='circularG'>"
                +"</div>"
                +"<div id='circularG_2' class='circularG'>"
                +"</div>"
                +"<div id='circularG_3' class='circularG'>"
                +"</div>"
                +"<div id='circularG_4' class='circularG'>"
                +"</div>"
                +"<div id='circularG_5' class='circularG'>"
                +"</div>"
                +"<div id='circularG_6' class='circularG'>"
                +"</div>"
                +"<div id='circularG_7' class='circularG'>"
                +"</div>"
                +"<div id='circularG_8' class='circularG'>"
                +"</div>"
                +"</div>");

                if (i == 0){

                    $.ajax({
                        type: 'GET',
                        url: '/beer-admin/users',
                        cache: false,
                        dataType: 'json',
                        async: true,
                        success: function(result) {
                            $("#circularG").hide();
                            $(tabs).children("div#container").children("div").eq(i).html(getTableUser(result));


                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.status + ' ' + jqXHR.responseText);
                        }
                    });

                }else if(i==1){

                    $.ajax({
                        type: 'GET',
                        url: '/beer-admin/beers',
                        cache: false,
                        dataType: 'json',
                        async: true,
                        success: function(result) {
                            $("#circularG").hide();

                            $(tabs).children("div#container").children("div").eq(i).html(getTableBeer(result));

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.status + ' ' + jqXHR.responseText);
                        }
                    });

                }else if(i == 2){

                    $.ajax({
                        type: 'GET',
                        url: '/beer-admin/sorts',
                        cache: false,
                        dataType: 'json',
                        async: true,
                        success: function(result) {
                            $("#circularG").hide();

                            $(tabs).children("div#container").children("div").eq(i).html(getTableSort(result));

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.status + ' ' + jqXHR.responseText);
                        }
                    });


                }else if(i == 3){

                    $.ajax({
                        type: 'GET',
                        url: '/beer-admin/country',
                        cache: false,
                        dataType: 'json',
                        async: true,
                        success: function(result) {
                            $("#circularG").hide();

                            $(tabs).children("div#container").children("div").eq(i).html(getTableCountry(result));

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.status + ' ' + jqXHR.responseText);
                        }
                    });


                }else if(i == 4){

                    $.ajax({
                        type: 'GET',
                        url: '/beer-admin/brewery',
                        cache: false,
                        dataType: 'json',
                        async: true,
                        success: function(result) {
                            $("#circularG").hide();

                            $(tabs).children("div#container").children("div").eq(i).html(getTableBrewery(result));

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.status + ' ' + jqXHR.responseText);
                        }
                    });


                }else if(i == 5){

                    $.ajax({
                        type: 'GET',
                        url: '/beer-admin/tag',
                        cache: false,
                        dataType: 'json',
                        async: true,
                        success: function(result) {
                            $("#circularG").hide();

                            $(tabs).children("div#container").children("div").eq(i).html(getTableTag(result));

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.status + ' ' + jqXHR.responseText);
                        }
                    });


                }

            }

            showPage = function(i){
                $(tabs).children("div#container").children("div").hide();
                $("div#form").children("div").each(function(){
                    $(this).css({"display":"none"});
                })
                loadData(i);
                $(tabs).children("div#container").children("div").eq(i).show();
                $(tabs).children("ul").children("li").removeClass("active");
                $(tabs).children("ul").children("li").eq(i).addClass("active");
            }



            showPage(0);

            $(tabs).children("ul").children("li").each(function(index, element){
                $(element).attr("data-page", i);
                i++;
            });

            $(tabs).children("ul").children("li").click(function(){
                showPage(parseInt($(this).attr("data-page")));
            });




        };
        return this.each(createTabs);
    };
})(jQuery);

$(document).ready(function(){
    $(".tabs").lightTabs();

});

function openForm(str){
    $("#"+str).css({"display":"block"});

    if (str === "add-brewery"){

        $.ajax({
            type: 'GET',
            url: '/beer-admin/country',
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                var str = "";

                for (i= 0; i < result["countries"].length; i++){

                    str += "<option value='"+result["countries"][i]["id"]+"'>"+ result["countries"][i]["name"] +"</option>"

                }
                $("#l-country").html(str);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

    }else if(str === "add-beer"){

        $.ajax({
            type: 'GET',
            url: '/beer-admin/brewery',
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                var str = "";

                for (i= 0; i < result["breweries"].length; i++){

                    str += "<option value='"+result["breweries"][i]["id"]+"'>"+ result["breweries"][i]["name"] +"</option>"

                }
                $("#l-brewery").html(str);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

        $.ajax({
            type: 'GET',
            url: '/beer-admin/sorts',
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                var str = "";

                for (i= 0; i < result["sorts"].length; i++){

                    str += "<option value='"+result["sorts"][i]["id"]+"'>"+ result["sorts"][i]["name"] +"</option>"

                }
                $("#l-sort").html(str);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

        $.ajax({
            type: 'GET',
            url: '/beer-admin/tag',
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                var str = "";

                for (i= 0; i < result["tags"].length; i++){

                    str += "<option value='"+result["tags"][i]["id"]+"'>"+ result["tags"][i]["name"] +"</option>"

                }
                $("#l-tags").html(str);
                $(".chosen-select").chosen();

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

    }

}



