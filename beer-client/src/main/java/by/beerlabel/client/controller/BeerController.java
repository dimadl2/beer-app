package by.beerlabel.client.controller;

import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.dto.NoteTO;
import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.dto.vo.Beer;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.*;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.enterprise.inject.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.ws.RequestWrapper;
import java.util.List;

/**
 * Created by dima on 30.5.15.
 */
@Controller
@SessionAttributes("beerId")
@RequestMapping("/beer")
public class BeerController {

    public static final String ADDNOTE = "addnote";
    @Autowired
    private BeerService beerService;

    @Autowired
    private BeerManage beerManage;

    @Autowired
    private NoteService noteService;

    @Autowired
    private UserService userService;

    @Autowired
    private TagService tagService;

    @Autowired
    private MarkService markService;

    private static final Logger LOG = Logger.getLogger(BeerController.class);


    @RequestMapping(value = "/{name}",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getBeerByName(@PathVariable String name) throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<BeerTO> beers = beerService.searchBeerByName(name);

        for (BeerTO beer : beers) {

            JSONObject beerObject = new JSONObject();
            beerObject.put("id", beer.getId());
            beerObject.put("name", beer.getName());
            beerObject.put("image", beer.getImgLink());

            array.add(beerObject);

        }


        data.put("beers", array);

        return data.toJSONString();

    }

    @RequestMapping(value = "/recent",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getRecentActivity() throws TechnicalException {

        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();


        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<BeerTO> beers = beerService.fetchRecentActivity(user.getUsername());

        for (BeerTO beer : beers) {

            JSONObject beerObject = new JSONObject();
            beerObject.put("id", beer.getId());
            beerObject.put("name", beer.getName());
            beerObject.put("image", beer.getImgLink());
            beerObject.put("mark", beer.getMarkImgLink());

            array.add(beerObject);

        }

        data.put("beers", array);

        return data.toJSONString();

    }


    @RequestMapping(value = "/recomended",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getRecomended() throws TechnicalException {

        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();


        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        List<BeerTO> beers = beerService.fetchRecomended(user.getUsername());

        for (BeerTO beer : beers) {

            JSONObject beerObject = new JSONObject();
            beerObject.put("id", beer.getId());
            beerObject.put("name", beer.getName());
            beerObject.put("image", beer.getImgLink());

            array.add(beerObject);

        }

        data.put("beers", array);

        return data.toJSONString();

    }

    @RequestMapping("/{id}")
    public ModelAndView getBeer(@PathVariable Long id,
                                @RequestParam(required = false) String action,
                                @RequestParam(required = false) String note) throws TechnicalException, ServiceException {

        System.out.println("OK");

        ModelAndView model = new ModelAndView();

        Beer beer = beerManage.fetchById(id);

        if (ADDNOTE.equals(action)){

            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

            Long idUser = userService.fetchIdByUsername(user.getUsername());
            NoteTO noteTO = new NoteTO(note);
            noteTO.setBeerId(id);
            noteTO.setUserId(idUser);
            noteService.save(noteTO);

        }

        if(SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            List<NoteTO> notes = noteService.fetchNotes(id, user.getUsername());
            model.addObject("notes", notes);
        }
        List<TagTO> tags = tagService.fetchTagsByBeerId(id);



        model.addObject("beerId", beer.getBeerTO().getId());
        model.addObject("beer", beer);
        model.addObject("tags", tags);
        model.setViewName("beerdesc");
        return model;
    }

    @RequestMapping(value = "/setmark", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String add(@RequestParam Long mark, HttpServletRequest req) throws TechnicalException, ServiceException, DAOException {

        HttpSession session = req.getSession();
        Long beerId = (Long) session.getAttribute("beerId");

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = userService.fetchIdByUsername(user.getUsername());

        System.out.println("OK");

        markService.setMark(beerId, userId, mark);

        return "";

    }


}
