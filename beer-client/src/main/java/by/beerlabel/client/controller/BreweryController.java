package by.beerlabel.client.controller;

import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.vo.Brewery;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BreweryManage;
import by.beerlabel.common.service.BreweryService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by dima on 1.6.15.
 */
@Controller
@RequestMapping("/breweries")
public class BreweryController {

    @Autowired
    BreweryManage breweryManage;

    @Autowired
    BreweryService breweryService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String sorts() throws TechnicalException, ServiceException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<Brewery> breweries = breweryManage.getAll();

        for (Brewery brewery : breweries) {

            JSONObject breweryObject = new JSONObject();

            breweryObject.put("name", brewery.getBreweryTO().getName());
            breweryObject.put("id", brewery.getBreweryTO().getId());
            breweryObject.put("country", brewery.getCountry().getName());

            array.add(breweryObject);

        }


        data.put("breweries", array);

        return data.toJSONString();
    }

    @RequestMapping(value = "/country/{id}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String brewery(@PathVariable Long id) throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<BreweryTO> breweries = breweryService.fetchBreweryByCountry(id);

        for (BreweryTO brewery : breweries) {

            JSONObject breweryObject = new JSONObject();

            breweryObject.put("name", brewery.getName());
            breweryObject.put("id", brewery.getId());

            array.add(breweryObject);

        }


        data.put("breweries", array);

        return data.toJSONString();
    }

}
