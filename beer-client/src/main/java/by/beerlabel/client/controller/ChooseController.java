package by.beerlabel.client.controller;

import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BeerService;
import by.beerlabel.common.util.Filter;
import com.sun.javafx.sg.PGShape;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * Created by dima on 1.6.15.
 */
@Controller
public class ChooseController {

    @Autowired
    BeerService beerService;


    @RequestMapping("/choose")
    public ModelAndView load(){

        ModelAndView model = new ModelAndView();
        model.setViewName("choose");

        return model;
    }


    @RequestMapping(value = "/choose/search",method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String search(@RequestParam(required = false) String name,
                         @RequestParam(required = false) Long country,
                         @RequestParam(required = false) Long brewery,
                         @RequestParam(required = false) Long sort,
                         @RequestParam(required = false) String alco,
                         @RequestParam(required = false) List<Long> tags) throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        Filter filter = new Filter();
        filter.setCountryId(country);
        filter.setBreweryId(brewery);
        filter.setSortId(sort);
        filter.setAlco(alco);
        filter.setTagsId(tags);
        filter.setName(name);

        List<BeerTO> beers = beerService.search(filter);

        for (BeerTO beer : beers) {

            JSONObject beerObject = new JSONObject();
            beerObject.put("id", beer.getId());
            beerObject.put("name", beer.getName());
            beerObject.put("image", beer.getImgLink());

            array.add(beerObject);
        }


        data.put("beers", array);

        return data.toJSONString();

    }

}
