package by.beerlabel.client.controller;

import by.beerlabel.common.dto.CountryTO;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.CountryService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by dima on 1.6.15.
 */
@Controller
@RequestMapping("/countries")
public class CountryController {


    @Autowired
    CountryService countryService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String countries() throws TechnicalException {


        JSONObject data = new JSONObject();

        JSONArray array = new JSONArray();

        List<CountryTO> countryTOs = countryService.getAll();

        for (CountryTO country : countryTOs) {

            JSONObject countryObject = new JSONObject();
            countryObject.put("name", country.getName());
            countryObject.put("id", country.getId());
            countryObject.put("abbr", country.getAbbr());

            array.add(countryObject);

        }

        data.put("countries", array);

        return data.toJSONString();
    }

}
