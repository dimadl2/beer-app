package by.beerlabel.client.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by dima on 30.5.15.
 */
@Controller
public class HomeController {

    @RequestMapping("/home")
    public ModelAndView loadPage(){

        ModelAndView model = new ModelAndView();
        model.setViewName("home");
        return model;

    }

}
