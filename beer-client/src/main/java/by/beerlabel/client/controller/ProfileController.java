package by.beerlabel.client.controller;

import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by dima on 31.5.15.
 */
@Controller
public class ProfileController {

    @Autowired
    private UserService userService;

    @RequestMapping("/profile")
    public ModelAndView loadPage() throws ServiceException, TechnicalException {

        ModelAndView model = new ModelAndView();

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long id = userService.fetchIdByUsername(user.getUsername());
        model.addObject("user", userService.fetchById(id));
        model.setViewName("profile");
        return model;

    }
}
