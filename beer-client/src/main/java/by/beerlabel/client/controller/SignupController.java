package by.beerlabel.client.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by dima on 2.6.15.
 */
@Controller

public class SignupController {

    @RequestMapping("/signup")
    public ModelAndView load(){

        ModelAndView model = new ModelAndView();

        model.setViewName("signup");

        return model;
    }

}
