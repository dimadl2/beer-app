<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 2.6.15
  Time: 0.30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<html>
<head>
  <title>Beer</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/beer_char.css'/>">
  <script type="text/javascript" src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
  <script src="<c:url value='/js/beer.js'/>"></script>
</head>

<body>
<div class="logo"><img src="<c:url value='/img/logo.png'/>"></div>
<div class="vyveska"><img src="<c:url value='/img/vyveska_beer.png'/>"></div>
<div class="text" align="center">Beer</div>

<div class="block" id="block"></div>
<div class="menu_button"><img src="<c:url value='/img/menu_button.png'/>" id="menu_button"></div>

<script>
  function chg(id){
    if (document.getElementById(id).src.indexOf("/beer-client/img/menu_button.png")>0){
      document.getElementById(id).src="/beer-client/img/check_button.png"}
    else{
      document.getElementById(id).src="/beer-client/img/menu_button.png"}
  }
</script>

<table id="menu" class="menu">
  <tr class="submenu"><td><a href="/beer-client/home">Home</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="/beer-client/profile">My Profile</a></td></tr>
  </sec:authorize>
  <tr class="submenu"><td><a href="/beer-client/choose">Choose Beer</a></td></tr>
  <tr class="submenu"><td><a href="contacts.html">Contacts</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="<c:url value='/logout'/>">Logout</a></td></tr>
  </sec:authorize>
  <sec:authorize access="isAnonymous()">
    <tr class="submenu"><td><a href="<c:url value='/login'/>">Login</a></td></tr>
  </sec:authorize>
</table>

<script>
  window.onload= function() {
    document.getElementById('menu_button').onclick = function() {
      chg('menu_button');
      openbox('menu', this);
      openbox('block', this);
      return false;
    };
  };
  function openbox(id, menu_button) {
    var div = document.getElementById(id);
    if(div.style.display == 'block') {
      div.style.display = 'none';
    }
    else {
      div.style.display = 'block';
    }
  }
</script>

<div class="right">
  <div class="main">
    <div class="img">
      <img src="/beer-client/<c:url value='${beer.beerTO.imgLink}'/>">
    </div>

    <div class="table">
      <table id="table">
        <tr>
          <td class="bold">Name</td>
          <td class="italic">${beer.beerTO.name}</td>
        </tr>
        <tr>
          <td class="bold">Style</td>
          <td class="italic">${beer.sort.name}</td>
        </tr>
        <tr>
          <td class="bold">Country</td>
          <td class="italic">${beer.brewery.country.name}</td>
        </tr>
        <tr>
          <td class="bold">Alcohol</td>
          <td class="italic">${beer.beerTO.alc}%</td>
        </tr>
        <tr>
          <td class="bold">Brewery</td>
          <td class="italic">${beer.brewery.breweryTO.name}</td>
        </tr>
      </table>
    </div>
  </div>

  <div class="description">
    <b>Description</b>
    <div class="border">
      ${beer.beerTO.description}
    </div>
  </div>

  <div class="tag">
    <b>Tags</b>
    <div class="tags">
      <c:forEach items="${tags}" var="tag">
        ${tag.name},
      </c:forEach>
    </div>
  </div>
</div>

<div class="left">
  <div class="marks">
    <b>Mark</b>
    <div class="mark">
      <form id="form-mark">
        <input type="radio" name="mark" class="mark-p" value="1">Like
        <input type="radio" name="mark" class="mark-p" value="2">Normal
        <input type="radio" name="mark" class="mark-p" value="3">Dislike
      </form>
    </div>
  </div>

  <div class="note">
    <b>Add comment</b></br>
    <form action="/beer-client/beer/${beer.beerTO.id}" method="POST">
      <textarea name="note" id="note" rows="4" cols="50" maxlength="200"></textarea></br>
      <input type="hidden" name="action" value="addnote">
      <input type="submit" value="Add" class="add">
    </form>
  </div>
<c:forEach items="${notes}" var="note">
  <div class="comment">
    ${note.description}
  </div>
</c:forEach>
</div>

</body>

</html>
