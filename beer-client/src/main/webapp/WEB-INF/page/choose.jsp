<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<html>
<head>
  <title>Choose beer</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/beer.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/scroll.css'/>">
  <link href="<c:url value='/choosen/docsupport/style.css'/>" rel="stylesheet" type="text/css">
  <link href="<c:url value='/choosen/docsupport/prism.css'/>" rel="stylesheet" type="text/css">
  <link href="<c:url value='/choosen/chosen.css'/>" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="<c:url value='/js/jquery-1.11.2.min.js'/>"></script>
  <script src="<c:url value='/js/script.js'/>"></script>
  <script src="<c:url value='/js/choose.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/choosen/chosen.jquery.js'/>"></script>
  <script type="text/javascript" src="<c:url value='/choosen/docsupport/prism.js'/>"></script>

</head>
<body>
<div class="logo"><img src="img/logo.png"></div>
<div class="vyveska"><img src="img/vyveska.png"></div>
<div class="text" align="center">Choose Beer</div>

<div class="block" id="block"></div>
<div class="menu_button"><img src="img/menu_button.png" id="menu_button"></div>

<script>
  function chg(id){
    if (document.getElementById(id).src.indexOf("img/menu_button.png")>0){
      document.getElementById(id).src="img/check_button.png"}
    else{
      document.getElementById(id).src="img/menu_button.png"}
  }
</script>

<table id="menu" class="menu">
  <tr class="submenu"><td><a href="/beer-client/home">Home</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="/beer-client/profile">My Profile</a></td></tr>
  </sec:authorize>
  <tr class="submenu"><td><a href="/beer-client/choose">Choose Beer</a></td></tr>
  <tr class="submenu"><td><a href="contacts.html">Contacts</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="<c:url value='/logout'/>">Logout</a></td></tr>
  </sec:authorize>
  <sec:authorize access="isAnonymous()">
    <tr class="submenu"><td><a href="<c:url value='/login'/>">Login</a></td></tr>
  </sec:authorize>
</table>

<script>
  window.onload= function() {
    document.getElementById('menu_button').onclick = function() {
      chg('menu_button');
      openbox('menu', this);
      openbox('block', this);
      return false;
    };
  };
  function openbox(id, menu_button) {
    var div = document.getElementById(id);
    if(div.style.display == 'block') {
      div.style.display = 'none';
    }
    else {
      div.style.display = 'block';
    }
  }
</script>

<form id="choose-form">
<div class="main">
  <div class="characteristics">
    <div class="country" id="char" align="center"><b>Country</b></br>
      <select required name="country" id="country" style="width: 180px;">
        <option disabled selected>-- Choose country --</option>
      </select>
    </div>
    <div id="char" align="center"><b>Brewery</b></br>
      <select name="brewery" id="brewery" style="width: 180px;">
        <option disabled selected>-- Choose brewery --</option>
      </select>
    </div>
    <div  id="char" align="center"><b>Sorts</b></br>
      <select name="sort" id="sorts" style="width: 180px;">
        <option disabled selected>-- Choose sorts --</option>
      </select>
    </div>
    <div id="char" align="center"><b>Alcohol</b></br>
      <select name="alco" id="alco" style="width: 180px;">
        <option disabled selected>-- Choose alcohol --</option>
        <option value="low">Low (3-4.5%)</option>
        <option value="medium">Medium (4.5-6%)</option>
        <option value="high">High (6-9%)</option>
      </select>
    </div>
    <div style=" width:100%; height:1px; clear:both;"></div>
  </div>
  <div class="add_tags">
    <div class="add_search">
      <table id="add_search">
        <tr id="add">
          <td><select id="l-tags" name="tags" data-placeholder="Choose tags..." style="width:350px;" multiple class="chosen-select"></select></td>
          <td><input id="name" type="text" name="name" size="38" placeholder="Start typing name..."></td>
        </tr>
      </table>
    </div>
    <div class="tags"></div>
  </div>
  <div class="result">

  </div>
</div>
  </form>
<div class="scroll" style="margin-top: 220px;">
  <div class="prev"></div>
  <ul class="list">
  </ul>
  <div class="next"></div>
</div>
</body>

</html>
