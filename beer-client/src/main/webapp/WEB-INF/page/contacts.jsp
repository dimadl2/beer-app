<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<html>
<head>
  <title>Contacts</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/contacts.css'/>">
</head>
<body>
<div class="logo"><img src="img/logo.png"></div>
<div class="vyveska"><img src="img/vyveska_contacts.png"></div>
<div class="text" align="center">Contacts</div>

<div class="block" id="block"></div>
<div class="menu_button"><img src="img/menu_button.png" id="menu_button"</div>

<script>
  function chg(id){
    if (document.getElementById(id).src.indexOf("img/menu_button.png")>0){
      document.getElementById(id).src="img/check_button.png"}
    else{
      document.getElementById(id).src="img/menu_button.png"}
  }
</script>

<table id="menu" class="menu">
  <tr class="submenu"><td><a href="/beer-client/home">Home</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="/beer-client/profile">My Profile</a></td></tr>
  </sec:authorize>
  <tr class="submenu"><td><a href="/beer-client/choose">Choose Beer</a></td></tr>
  <tr class="submenu"><td><a href="contacts.html">Contacts</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="<c:url value='/logout'/>">Logout</a></td></tr>
  </sec:authorize>
  <sec:authorize access="isAnonymous()">
    <tr class="submenu"><td><a href="<c:url value='/login'/>">Login</a></td></tr>
  </sec:authorize>
</table>

<script>
  window.onload= function() {
    document.getElementById('menu_button').onclick = function() {
      chg('menu_button');
      openbox('menu', this);
      openbox('block', this);
      return false;
    };
  };
  function openbox(id, menu_button) {
    var div = document.getElementById(id);
    if(div.style.display == 'block') {
      div.style.display = 'none';
    }
    else {
      div.style.display = 'block';
    }
  }
</script>

<div class="table">
  <table id="table" align="center">
    <tr>
      <td class="bold">Project</td>
      <td class="italic">Beer Label</td>
    </tr>
    <tr>
      <td class="bold">Developers</td>
      <td class="italic"><a href="http://vk.com/id20704415">Dima Dydyshko</a>,
        <a href="http://vk.com/katebogdanovich">Kate Bogdanovich</a></td>
    </tr>
    <tr>
      <td class="bold">E-mail</td>
      <td class="italic">beerlabel_by@gmail.com</td>
    </tr>

  </table>
</div>

</body>

</html>
