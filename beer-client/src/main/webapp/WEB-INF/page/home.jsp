<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 30.5.15
  Time: 20.54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<html>
<head>
  <title>Home</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/home.css'/>">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/scroll.css'/>">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script src="<c:url value='/js/script.js'/>"></script>

</head>
<body>
<div class="logo"><img src="img/logo.png"></div>
<div class="vyveska"><img src="img/vyveska_home.png"></div>
<div class="text" align="center">Beer Label</div>

<div class="block" id="block"></div>
<div class="menu_button"><img src="img/menu_button.png" id="menu_button"></div>

<script type="text/javascript">
  function chg(id){
    if (document.getElementById(id).src.indexOf("img/menu_button.png")>0){
      document.getElementById(id).src="img/check_button.png"}
    else{
      document.getElementById(id).src="img/menu_button.png"}
  }
</script>

<table id="menu" class="menu">
  <tr class="submenu"><td><a href="/beer-client/home">Home</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="/beer-client/profile">My Profile</a></td></tr>
  </sec:authorize>
  <tr class="submenu"><td><a href="/beer-client/choose">Choose Beer</a></td></tr>
  <tr class="submenu"><td><a href="contacts.html">Contacts</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="<c:url value='/logout'/>">Logout</a></td></tr>
  </sec:authorize>
  <sec:authorize access="isAnonymous()">
  <tr class="submenu"><td><a href="<c:url value='/login'/>">Login</a></td></tr>
  </sec:authorize>
</table>

<script type="text/javascript">
  window.onload= function() {
    document.getElementById('menu_button').onclick = function() {
      chg('menu_button');
      openbox('menu', this);
      openbox('block', this);
      return false;
    };
  };
  function openbox(id, menu_button) {
    var div = document.getElementById(id);
    if(div.style.display == 'block') {
      div.style.display = 'none';
    }
    else {
      div.style.display = 'block';
    }
  };
</script>

<div class="main" align="center">
  <div class="quote">
    <em>«Beer is proof that God loves us and wants us to be happy.»</em>
  </div>
  <div class="author">Benjamin Franklin</div></br>
  <div class="find">
    <form method="get" target="_blank">
      <input  type="search" name="search" id="search" value="Find a beer" onfocus="if (this.value == 'Find a beer') this.value = '';"
             onblur="if (this.value == '') this.value = 'Find a beer';">
    </form>
  </div>
</div>

<div class="scroll">
  <div class="prev"></div>
  <ul class="list">
    <li><img src="<c:url value='/img/beer_bottle1.png'/>"/></li>
  </ul>
  <div class="next"></div>
</div>
</body>

</html>
