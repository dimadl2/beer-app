<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 31.5.15
  Time: 8.30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec"
           uri="http://www.springframework.org/security/tags"%>
<html>
<head>
  <title>Home</title>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <link rel="stylesheet" type="text/css" href="<c:url value='/css/profile.css'/>">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
  <script src="<c:url value='/js/profile.js'/>"></script>
</head>
<body>
<div class="logo"><img src="img/logo.png"></div>
<div class="vyveska"><img src="img/vyveska_profile.png"></div>
<div class="text" align="center">My Profile</div>

<div class="block" id="block"></div>
<div class="menu_button"><img src="img/menu_button.png" id="menu_button"></div>

<script>
  function chg(id){
    if (document.getElementById(id).src.indexOf("img/menu_button.png")>0){
      document.getElementById(id).src="img/check_button.png"}
    else{
      document.getElementById(id).src="img/menu_button.png"}
  }
</script>

<table id="menu" class="menu">
  <tr class="submenu"><td><a href="/beer-client/home">Home</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="/beer-client/profile">My Profile</a></td></tr>
  </sec:authorize>
  <tr class="submenu"><td><a href="/beer-client/choose">Choose Beer</a></td></tr>
  <tr class="submenu"><td><a href="contacts.html">Contacts</a></td></tr>
  <sec:authorize access="isAuthenticated()">
    <tr class="submenu"><td><a href="<c:url value='/logout'/>">Logout</a></td></tr>
  </sec:authorize>
  <sec:authorize access="isAnonymous()">
    <tr class="submenu"><td><a href="<c:url value='/login'/>">Login</a></td></tr>
  </sec:authorize>
</table>

<script>
  window.onload= function() {
    document.getElementById('menu_button').onclick = function() {
      chg('menu_button');
      openbox('menu', this);
      openbox('block', this);
      return false;
    };
  };
  function openbox(id, menu_button) {
    var div = document.getElementById(id);
    if(div.style.display == 'block') {
      div.style.display = 'none';
    }
    else {
      div.style.display = 'block';
    }
  }
</script>

<div class="main">
  <div class="table">
    <table id="table">
      <tr>
        <td class="ava" rowspan="4"><img src="img/ava.png" alt="it's you" style="width: 150px;"></td>
        <td class="bold">User name:</td>
        <td class="italic">${user.username}</td>
      </tr>
      <tr>
        <td class="bold">Date of birth:</td>
        <td class="italic">${user.dateBirth}</td>
      </tr>
      <tr>
        <td class="bold">Email:</td>
        <td class="italic">${user.email}</td>
      </tr>
      <tr>
        <td class="bold">Preferences:</td>
        <td class="italic">Stout, IPA, Red Ale</td>
      </tr>
    </table>
  </div>
</div>

<div class="activity">
  <div class="left">
    <b>Recent Activity</b>
    <div class="rating">
    </div>
  </div>
  <div class="left">
    <b>Recommendations</b>
    <div class="recom">
    </div>
  </div>
</div>


</body>

</html>
