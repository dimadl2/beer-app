/**
 * Created by dima on 1.6.15.
 */
$(document).ready(function(){



    $("#country").change(function(){

        search();

        $.ajax({
            type: 'GET',
            url: '/beer-client/breweries/country/' + $("#country").val(),
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                list = "<option disabled selected>-- Choose brewery --</option>";

                for (i = 0; i < result["breweries"].length; i++){
                    list += "<option value='" + result["breweries"][i]["id"]+"'>" +
                    result["breweries"][i]["name"] +
                    "</option>";
                }

                $("#brewery").html(list);


            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

        $.ajax({
            type: 'GET',
            url: '/beer-admin/sorts/country/' + $(this).val(),
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                list = "<option disabled selected>-- Choose sorts --</option>";

                for (i = 0; i < result["sorts"].length; i++){
                    list += "<option  value='" + result["sorts"][i]["id"]+"'>" +
                    result["sorts"][i]["name"] +
                    "</option>";
                }

                $("#sorts").html(list);


            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });



    });

    $("#brewery").change(function(){

        search();

        $.ajax({
            type: 'GET',
            url: '/beer-admin/sorts/brewery/' + $(this).val(),
            cache: false,
            dataType: 'json',
            async: true,
            success: function(result) {

                list = "<option disabled selected>-- Choose sorts --</option>";

                for (i = 0; i < result["sorts"].length; i++){
                    list += "<option  value='" + result["sorts"][i]["id"]+"'>" +
                    result["sorts"][i]["name"] +
                    "</option>";
                }

                $("#sorts").html(list);


            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.status + ' ' + jqXHR.responseText);
            }
        });

    });

    $.ajax({
        type: 'GET',
        url: '/beer-client/countries',
        cache: false,
        dataType: 'json',
        async: true,
        success: function(result) {

            list = $("#country").html();

            for (i = 0; i < result["countries"].length; i++){
                list += "<option class='ch-country' value='" + result["countries"][i]["id"]+"'>" +
                result["countries"][i]["name"] + " (" + result["countries"][i]["abbr"] + ")" +
                "</option>";
            }

            $("#country").html(list);


        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }
    });

    $.ajax({
        type: 'GET',
        url: '/beer-admin/sorts',
        cache: false,
        dataType: 'json',
        async: true,
        success: function(result) {

            list = $("#sorts").html();

            for (i = 0; i < result["sorts"].length; i++){
                list += "<option  value='" + result["sorts"][i]["id"]+"'>" +
                result["sorts"][i]["name"] +
                "</option>";
            }

            $("#sorts").html(list);


        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }
    });

    $.ajax({
        type: 'GET',
        url: '/beer-admin/tag',
        cache: false,
        dataType: 'json',
        async: true,
        success: function(result) {

            var str = "";

            for (i= 0; i < result["tags"].length; i++){

                str += "<option value='"+result["tags"][i]["id"]+"'>"+ result["tags"][i]["name"] +"</option>"

            }
            $("#l-tags").html(str);
            $(".chosen-select").chosen();

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }
    });

    $.ajax({
        type: 'GET',
        url: '/beer-admin/brewery',
        cache: false,
        dataType: 'json',
        async: true,
        success: function(result) {

            list = $("#brewery").html();

            for (i = 0; i < result["breweries"].length; i++){
                list += "<option value='" + result["breweries"][i]["id"]+"'>" +
                result["breweries"][i]["name"] +
                "</option>";
            }

            $("#brewery").html(list);
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }
    });

    $("#sorts").change(function(){
        search();
    });

    $("#alco").change(function(){
        search();
    });

    $("#name").keyup(function(){
        search();
    });

    $("#l-tags").change(function(){
        search();
    })




});

function search(){

    var data = $("#choose-form").serialize();

    $.ajax({
        type: 'GET',
        url: '/beer-client/choose/search',
        cache: false,
        dataType: 'json',
        data: data,
        async: true,
        success: function(result) {

            if (result["beers"].length !== 0){

                $(".scroll").css({"display": "block"});

                var list = "";

                for (var i = 0; i<result['beers'].length; i++){

                    list += "<li><img src='"+result['beers'][i]['image']+"'/></li>";

                }

                $(".list").html(list);

            }else{
                $(".scroll").css({"display": "none"});
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }
    });

}

