/**
 * Created by dima on 31.5.15.
 */
$(document).ready(function(){

    $.ajax({
        type: 'GET',
        url: '/beer-client/beer/recent',
        cache: false,
        dataType: 'json',
        async: true,
        success: function (result) {

            var list = "";

            for (i = 0; i<result["beers"].length; i++){

                list += "<div class='rating_1'>"+
                    "<div><img src='"+result["beers"][i]["image"]+"'></div>"+
                    "<div class='mark'><img src='"+result["beers"][i]["mark"]+"'></div>"+
                    "</div>";
            }

            $(".rating").html(list);


        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }

    });

    $.ajax({
        type: 'GET',
        url: '/beer-client/beer/recomended',
        cache: false,
        dataType: 'json',
        async: true,
        success: function (result) {

            var list = "";

            for (i = 0; i<result["beers"].length; i++){

                list += "<div class='rating_1'>"+
                "<div><a href='/beer-client/beer/"+result["beers"][i]["id"]+"'><img src='"+result['beers'][i]['image']+"'/></a></div>"+
                "</div>";
            }

            $(".recom").html(list);


        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status + ' ' + jqXHR.responseText);
        }

    })

})
