$(document).ready(function() {

	onMoving			= false;
	item				= $('.list li');
	itemReverse		= item.get().reverse();
	itemSize			= item.size();
	itemDisplayed	= 7;
	itemToSlide		= itemDisplayed * 78 + 37;
	currentSlide	= 1;
	page				= Math.round(itemSize/itemDisplayed)+1;

	//Initialize

	$("#search").keyup(function() {

		var search = $(this).val();
		search = $.trim(search);

		if (search === ""){
			$(".scroll").css({"display":"none"});
			return false;
		}

		$.ajax({
			type: 'GET',
			url: '/beer-client/beer/' + $(this).val(),
			cache: false,
			dataType: 'json',
			async: true,
			success: function (result) {

				if (result["beers"].length !== 0){

					$(".scroll").css({"display": "block"});

					var list = "";

					for (var i = 0; i<result['beers'].length; i++){

						list += "<li><a href='/beer-client/beer/"+result["beers"][i]["id"]+"'><img src='"+result['beers'][i]['image']+"'/></a></li>";

					}

					$(".list").html(list);

					onMoving			= false;
					item				= $('.list li');
					itemReverse		= item.get().reverse();
					itemSize			= item.size();
					itemDisplayed	= 7;
					itemToSlide		= itemDisplayed * 78 + 37;
					currentSlide	= 1;
					page				= Math.round(itemSize/itemDisplayed)+1;

				}




			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + ' ' + jqXHR.responseText);
			}

		})
	});



	//Next button clicked
	$('.next').click(function() {

		if(onMoving || currentSlide >= page) return false;
		onMoving = true, currentSlide++;		
		
		$.each(item, function() {
			
			var i 	 = $(this).index();
			var delay = i * 100;

			window.setTimeout(function (index) {
				return function () {
					item.eq(index).stop().animate({ 'right' : '+='+itemToSlide+'px' }, function() {
               	if(index >= itemSize-1) onMoving = false;
					});
				};
			} (i), delay);
		});
	});
	
	//Previous button clicked
	$('.prev').click(function() {

		if(onMoving || currentSlide == 1) return false;
		onMoving = true, currentSlide--;
		
		$.each(itemReverse, function() {
			
			var i 	 = $(this).index();
			var delay = i * 100;
			
			window.setTimeout(function (index) {
				return function () {
					$(itemReverse).eq(index).stop().animate({ 'right' : '-='+itemToSlide+'px' }, function() {
						if(index >= 0) onMoving = false;
					});
				};
			} (i), delay);      
		});
	});
});