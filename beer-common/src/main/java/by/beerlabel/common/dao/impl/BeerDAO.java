package by.beerlabel.common.dao.impl;

import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.CountryTO;
import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.util.Filter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;



@Repository("beerDAO")
public class BeerDAO extends AbstractDAO<BeerTO> {

	private static final String SQL_FETCH_BY_ID = "SELECT beer.name, beer.description, beer.alc, beer.img_link, sort_beer.sort_name, "
			+ "brewery.name as brewery, country.name as country, country.abbr "
			+ "FROM beer JOIN sort_beer USING(sort_beer_id) "
			+ "JOIN brewery USING(brewery_id) "
			+ "JOIN country USING(country_id) " + "WHERE beer_id = ?";
	private static final String COL_BEER_ID = "beer_id";
	private static final String COL_IMG_LINK = "img_link";
	private static final String COL_ALC = "alc";
	private static final String COL_DESCRIPTION = "description";
	private static final String COL_NAME = "name";
	private static final String COL_SORT_NAME = "sort_name";
	private static final String COL_ABBR = "abbr";
	private static final String COL_BREWERY = "brewery";
	private static final String COL_COUNTRY = "country";
	private static final String SELECT_BEER_BY_NAME = "SELECT beer_id, name, img_link  FROM beer WHERE upper(beer.name) LIKE upper(?) AND rownum<=20";
	private static final String SELECT_RECOMENDED_BEER = "SELECT * FROM (SELECT beer_id, name, img_link " +
			"FROM beer " +
			"WHERE beer.sort_beer_id IN " +
				"(SELECT sort_beer_id " +
					"FROM user_pref JOIN user_app USING (user_app_id) " +
					"WHERE user_app.username = ?) " +
			"OR beer.sort_beer_id IN " +
				"(SELECT DISTINCT sort_beer_id " +
					"FROM user_mark JOIN user_app USING(user_app_id) " +
					"JOIN beer USING(beer_id) WHERE username=? AND mark_id IN (1,2)) " +
			"AND rownum < 5 ORDER BY taste_count DESC) WHERE beer_id NOT IN (SELECT beer_id FROM user_mark JOIN user_app USING(user_app_id) WHERE username=?)";

	public static final String SELECT_RECENT_ACTIVITY = "SELECT * FROM (SELECT beer.beer_id, beer.img_link as beer_image, mark.img_link as mark_img FROM user_mark JOIN beer ON user_mark.beer_id = beer.beer_id JOIN mark ON user_mark.mark_id = mark.mark_id JOIN user_app ON user_mark.user_app_id = user_app.user_app_id WHERE user_app.username = ? ORDER BY user_mark.time DESC) WHERE rownum < 6 ";
	public static final String SQL_PREFIX_SEARCH = "SELECT DISTINCT beer.beer_id, beer.img_link FROM beer";
	public static final String SQL_JOIN_BREWERY = " JOIN brewery ON beer.brewery_id=brewery.brewery_id AND beer.brewery_id=?";
	public static final String SQL_JOIN_COUNTRY = " JOIN brewery ON beer.brewery_id=brewery.brewery_id JOIN country ON brewery.country_id=country.country_id AND brewery.country_id=?";
	public static final String SQL_JOIN_SORT = " JOIN sort_beer ON beer.sort_beer_id=sort_beer.sort_beer_id AND beer.sort_beer_id=?";
	public static final String SQL_JOIN_TAGS_PREFIX = " JOIN tag_beer ON beer.beer_id=tag_beer.beer_id AND tag_beer.tag_id IN (";

	private static Logger log = Logger.getLogger(BeerDAO.class);

	public List<BeerTO> searchBeerByName(String name) throws DAOException {

		List<BeerTO> beers = new LinkedList<BeerTO>();

		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			conn = dataSource.getConnection();
			st = conn.prepareStatement(SELECT_BEER_BY_NAME);
			st.setString(1, "%" + name + "%");
			rs = st.executeQuery();

			while (rs.next()) {

				BeerTO beer = new BeerTO();
				beer.setId(rs.getLong(1));
				beer.setName(rs.getString(2));
				beer.setImgLink(rs.getString(3));

				beers.add(beer);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.closeResources(conn, st, rs);
		}

		return beers;

	}

	public List<BeerTO> fetchRecomendedBeer(String username)
			throws DAOException {

		Connection connection = null;
		PreparedStatement st = null;
		ResultSet resultSet = null;

		List<BeerTO> beers = new LinkedList<BeerTO>();

		try {

			connection = dataSource.getConnection();
			st = connection.prepareStatement(SELECT_RECOMENDED_BEER);
			st.setString(1, username);
			st.setString(2, username);
			st.setString(3, username);

			resultSet = st.executeQuery();

			while (resultSet.next()) {

				BeerTO beer = new BeerTO();

				beer.setId(resultSet.getLong(COL_BEER_ID));
				beer.setName(resultSet.getString(COL_NAME));
				beer.setImgLink(resultSet.getString(COL_IMG_LINK));

				beers.add(beer);

			}

		} catch (SQLException e) {

			throw new DAOException(e);

		} finally {

			DAOUtils.closeResources(connection, st, resultSet);

		}

		return beers;

	}


	public List<BeerTO> fetchRecenActivity(String username) throws DAOException {

		List<BeerTO> beers = new LinkedList<BeerTO>();

		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			conn = dataSource.getConnection();
			st = conn.prepareStatement(SELECT_RECENT_ACTIVITY);
			st.setString(1, username);

			rs = st.executeQuery();

			while (rs.next()) {

				BeerTO beer = new BeerTO();
				beer.setId(rs.getLong(1));
				beer.setImgLink(rs.getString(2));
				beer.setMarkImgLink(rs.getString(3));

				beers.add(beer);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.closeResources(conn, st, rs);
		}

		return beers;

	}

	public List<BeerTO> search(Filter filter) throws DAOException {

		List<BeerTO> beers = new LinkedList<BeerTO>();

		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		try {
			conn = dataSource.getConnection();
			st = conn.prepareStatement(generateSearchQuery(filter));

			int i = 0;

			if (filter.getBreweryId() != null) {

				i++;
				st.setLong(i, filter.getBreweryId());

			}else if (filter.getCountryId() != null){

				i++;
				st.setLong(i, filter.getCountryId());

			}

			if (filter.getSortId() != null){

				i++;
				st.setLong(i, filter.getSortId());

			}

			if (filter.getTagsId() != null){

				List<Long> tagsId = filter.getTagsId();

				for (int j = 0; j<tagsId.size(); j++) {

					i+=1;
					st.setLong(i, tagsId.get(j));

				}
			}

			rs = st.executeQuery();

			while (rs.next()) {

				BeerTO beer = new BeerTO();
				beer.setId(rs.getLong(1));
				beer.setImgLink(rs.getString(2));

				beers.add(beer);

			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.closeResources(conn, st, rs);
		}

		return beers;

	}

	private String generateSearchQuery(Filter filter){

		StringBuilder query = new StringBuilder(SQL_PREFIX_SEARCH);

		if (filter.getBreweryId() != null) {

			query.append(SQL_JOIN_BREWERY);

		}else if (filter.getCountryId() != null){

			query.append(SQL_JOIN_COUNTRY);

		}

		if (filter.getSortId() != null){

			query.append(SQL_JOIN_SORT);
		}

		if (filter.getTagsId() != null){

			query.append(SQL_JOIN_TAGS_PREFIX);

			List<Long> tagsId = filter.getTagsId();

			for (int i = 0; i<tagsId.size(); i++) {


				query.append("?");

				if (i != tagsId.size()-1){
					query.append(",");
				}


			}

			query.append(")");


		}

		boolean flagAnd = false;

		if (filter.getName() != null || filter.getAlco()!=null){
			query.append(" WHERE");

			if (filter.getName() != null){

				query.append(" upper(beer.name) LIKE upper('%").append(filter.getName()).append("%')");

				flagAnd = true;

			}

			if (filter.getAlco() != null){


				if (flagAnd){
					query.append(" AND ");
				}

				if ("low".equals(filter.getAlco())){

					query.append(" beer.alc BETWEEN 0 AND 4.5");

				}else if ("medium".equals(filter.getAlco())){
					query.append(" beer.alc BETWEEN 4.5 AND 7");
				}else if ("high".equals(filter.getAlco())){
					query.append(" beer.alc BETWEEN 7 AND 12");
				}

			}

		}


		log.info(query.toString());

		return query.toString();
	}


}
