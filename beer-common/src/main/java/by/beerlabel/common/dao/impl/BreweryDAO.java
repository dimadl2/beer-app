package by.beerlabel.common.dao.impl;

import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.vo.Brewery;
import by.beerlabel.common.exception.DAOException;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Repository("breweryDAO")
public class BreweryDAO extends AbstractDAO<BreweryTO> {

    public static final String SQL_FETCH_BREWERY_BY_COUNTRY = "SELECT brewery.name, brewery.brewery_id FROM brewery JOIN country USING(country_id) WHERE country_id=?";

    public List<BreweryTO> fetchBreweryByCountry(Long countryId) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatemen = null;
        ResultSet rs = null;

        List<BreweryTO> breweries = new LinkedList<>();

        try {
            connection = dataSource.getConnection();
            preparedStatemen = connection.prepareCall(SQL_FETCH_BREWERY_BY_COUNTRY);
            preparedStatemen.setLong(1, countryId);

            rs = preparedStatemen.executeQuery();

            while(rs.next()){

                BreweryTO brewery = new BreweryTO();
                brewery.setName(rs.getString(1));
                brewery.setId(rs.getLong(2));

                breweries.add(brewery);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(connection, preparedStatemen, rs);
        }

        return breweries;

    }

}
