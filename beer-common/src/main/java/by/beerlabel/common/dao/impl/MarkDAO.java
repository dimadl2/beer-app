package by.beerlabel.common.dao.impl;


import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.MarkTO;
import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.exception.DAOException;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Repository("markDAO")
public class MarkDAO extends AbstractDAO<MarkTO> {

    public static final String SQL_ATTACH_MARK_TO_BEER = "INSERT INTO user_mark VALUES (?, ?, ?, NULL)";
    public static final String SQL_DELETE_MARK = "DELETE FROM user_mark WHERE user_app_id=? and beer_id=?";

    public void setMark(Long beerId, Long userId, Long markId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;

        try {

            connection = dataSource.getConnection();

            statement = connection.prepareStatement(SQL_DELETE_MARK);
            statement.setLong(1, userId);
            statement.setLong(2, beerId);
            statement.executeUpdate();

            statement = connection.prepareStatement(SQL_ATTACH_MARK_TO_BEER);
            statement.setLong(1, userId);
            statement.setLong(2, markId);
            statement.setLong(3, beerId);

            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {

            DAOUtils.closeResources(connection, statement, null);

        }
    }


}
