package by.beerlabel.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.NoteTO;
import by.beerlabel.common.exception.DAOException;
import org.springframework.stereotype.Repository;


@Repository("noteDAO")
public class NoteDAO extends AbstractDAO<NoteTO> {
	
	private static final String COL_NOTE_DESC = "note_desc";
	private static final String SQL_FETCH_NOTE = "SELECT note_desc FROM note JOIN user_app USING(user_app_id) WHERE beer_id = ? and username = ?";

	public List<NoteTO> fetchNote(Long beerId, String username) throws DAOException {
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		List<NoteTO> notes = new LinkedList<NoteTO>();
		
		try {
			
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_NOTE);
			statement.setLong(1, beerId);
			statement.setString(2, username);
			
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				
				NoteTO note = new NoteTO(resultSet.getString(COL_NOTE_DESC));
				notes.add(note);
				
			}
			
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			
			DAOUtils.closeResources(connection, statement, resultSet);
		}
		
		return notes;
		
	}

}
