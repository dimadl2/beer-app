package by.beerlabel.common.dao.impl;

import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.exception.DAOException;
import org.springframework.stereotype.Repository;
import sun.misc.Sort;

import java.net.ConnectException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


@Repository("sortDAO")
public class SortDAO extends AbstractDAO<SortTO> {

    public static final String SQL_FETCH_SORT_BY_COUNTRY = "SELECT DISTINCT sort_name, sort_beer_id FROM beer JOIN sort_beer USING(sort_beer_id) JOIN brewery USING(brewery_id) JOIN country USING(country_id) WHERE country_id=?";
    public static final String SQL_FETCH_SORT_BY_BREWERY = "SELECT DISTINCT sort_name, sort_beer_id FROM beer JOIN sort_beer USING(sort_beer_id) JOIN brewery USING(brewery_id)  WHERE brewery_id=?";

    public void delete(List<Long> sortIds) throws DAOException {

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = dataSource.getConnection();

            StringBuilder query = new StringBuilder("DELETE FROM sort_beer WHERE sort_beer_id IN(");
            for (int i = 0; i < sortIds.size(); i++){

                query.append("?");
                if (i != sortIds.size()-1){
                    query.append(",");
                }

            }

            query.append(")");

            st = conn.prepareCall(query.toString());
            for (int i = 0; i < sortIds.size(); i++){
                st.setLong(i+1, sortIds.get(i));
            }

            st.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(conn, st, null);
        }

    }

    public List<SortTO> fetchSortsByCountry(Long countryId) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatemen = null;
        ResultSet rs = null;

        List<SortTO> sorts = new LinkedList<>();

        try {
            connection = dataSource.getConnection();
            preparedStatemen = connection.prepareCall(SQL_FETCH_SORT_BY_COUNTRY);
            preparedStatemen.setLong(1, countryId);

            rs = preparedStatemen.executeQuery();

            while(rs.next()){

                SortTO sort = new SortTO();
                sort.setName(rs.getString(1));
                sort.setId(rs.getLong(2));

                sorts.add(sort);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(connection, preparedStatemen, rs);
        }

        return sorts;

    }

    public List<SortTO> fetchSortsByBrewery(Long breweryId) throws DAOException {

        Connection connection = null;
        PreparedStatement preparedStatemen = null;
        ResultSet rs = null;

        List<SortTO> sorts = new LinkedList<>();

        try {
            connection = dataSource.getConnection();
            preparedStatemen = connection.prepareCall(SQL_FETCH_SORT_BY_BREWERY);
            preparedStatemen.setLong(1, breweryId);

            rs = preparedStatemen.executeQuery();

            while(rs.next()){

                SortTO sort = new SortTO();
                sort.setName(rs.getString(1));
                sort.setId(rs.getLong(2));

                sorts.add(sort);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            DAOUtils.closeResources(connection, preparedStatemen, rs);
        }

        return sorts;

    }

}
