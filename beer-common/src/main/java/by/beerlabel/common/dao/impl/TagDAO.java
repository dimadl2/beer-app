package by.beerlabel.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Repository;

import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.exception.DAOException;

@Repository("tagDAO")
public class TagDAO extends AbstractDAO<TagTO> {


	private static final String COL_TAG_NAME = "tag_name";
	private static final String SQL_FETCH_TAGS_BY_NEWS = "SELECT tag_name "
			+ "FROM tag_beer "
			+ "JOIN tag USING(tag_id) "
			+ "WHERE beer_id = ?";
	public static final String SQL_ATTACH_TAG_TO_BEER = "INSERT INTO tag_beer VALUES ( ?, ?)";

	public List<TagTO> fetchTagsByNewsId(Long id) throws DAOException{
		
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		
		List<TagTO> tags = new LinkedList<TagTO>();
		
		try {
			
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_FETCH_TAGS_BY_NEWS);
			statement.setLong(1, id);
			
			resultSet = statement.executeQuery();
			
			while(resultSet.next()){
				
				TagTO tag = new TagTO(resultSet.getString(COL_TAG_NAME));
				tags.add(tag);
				
			}
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			
			DAOUtils.closeResources(connection, statement, resultSet);
			
		}
		
		return tags;
		
		
	}

	public void attachTagToBeer(Long beer, Long tag) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;

		List<TagTO> tags = new LinkedList<TagTO>();

		try {

			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_ATTACH_TAG_TO_BEER);
			statement.setLong(1, beer);
			statement.setLong(2, tag);

			statement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {

			DAOUtils.closeResources(connection, statement, null);

		}

	}

}
