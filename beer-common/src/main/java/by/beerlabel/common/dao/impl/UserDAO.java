package by.beerlabel.common.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Repository;

import by.beerlabel.common.dao.AbstractDAO;
import by.beerlabel.common.dao.DAOUtils;
import by.beerlabel.common.dto.UserTO;
import by.beerlabel.common.exception.DAOException;

@Repository("userDAO")
public class UserDAO extends AbstractDAO<UserTO>{

	private static final String SQL_GET_PASSWORD_BY_USERNAME = "SELECT user_app_id FROM user_app WHERE username = ?";

	public Long fetchIdByUsername(String username) throws DAOException {

		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		Long id = null;
		
		try {
			connection = dataSource.getConnection();
			statement = connection.prepareStatement(SQL_GET_PASSWORD_BY_USERNAME);
			statement.setString(1, username);
			
			resultSet = statement.executeQuery();
			
			resultSet.next();
			
			id = resultSet.getLong(1);
			
			
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
				DAOUtils.closeResources(connection, statement, resultSet);
		}
		
		return id;

	}

	public List<UserTO> fetchLimitUsers(int startPosition, int endPosition) throws DAOException {

		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;

		List<UserTO> users = new LinkedList<>();

		try {
			connection = dataSource.getConnection();
			st = connection.prepareCall("");
		} catch (SQLException e) {
			throw new DAOException(e);
		}finally {
			DAOUtils.closeResources(connection, st, rs);
		}

		return users;

	}
	
}
