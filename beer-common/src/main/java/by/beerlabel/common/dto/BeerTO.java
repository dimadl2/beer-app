package by.beerlabel.common.dto;


import by.beerlabel.common.annotation.Column;
import by.beerlabel.common.annotation.Table;

@Table(name = "beer")
public class BeerTO extends Entity {

	private static final long serialVersionUID = 1L;

	@Column(name = "name")
	private String name;

	@Column(name = "sort_beer_id")
	private Long sortId;

	@Column(name = "alc")
	private double alc;

	@Column(name = "description")
	private String description;

	@Column(name = "img_link")
	private String imgLink;
	
	@Column(name="brewery_id")
	private Long breweryId;

	private String markImgLink;

	public String getMarkImgLink() {
		return markImgLink;
	}

	public void setMarkImgLink(String markImgLink) {
		this.markImgLink = markImgLink;
	}

	public Long getBreweryId() {
		return breweryId;
	}

	public void setBreweryId(Long breweryId) {
		this.breweryId = breweryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getSortId() {
		return sortId;
	}

	public void setSortId(Long sortId) {
		this.sortId = sortId;
	}

	public double getAlc() {
		return alc;
	}

	public void setAlc(double alc) {
		this.alc = alc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImgLink() {
		return imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

}
