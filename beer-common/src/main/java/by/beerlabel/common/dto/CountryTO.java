package by.beerlabel.common.dto;


import by.beerlabel.common.annotation.Column;
import by.beerlabel.common.annotation.Table;

@Table(name="country")
public class CountryTO extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="name")
	private String name;
	
	@Column(name="abbr")
	private String abbr;
	
	public CountryTO(){
		
	}

	public CountryTO(String name, String abbr) {
		super();
		this.name = name;
		this.abbr = abbr;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}
	
	

}
