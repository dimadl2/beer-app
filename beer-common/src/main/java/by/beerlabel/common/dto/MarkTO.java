package by.beerlabel.common.dto;


import by.beerlabel.common.annotation.Column;
import by.beerlabel.common.annotation.Table;

@Table(name = "mark")
public class MarkTO extends Entity {

	private static final long serialVersionUID = 1L;

	@Column(name = "mark_desc")
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
