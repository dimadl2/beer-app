package by.beerlabel.common.dto;


import by.beerlabel.common.annotation.Column;
import by.beerlabel.common.annotation.Table;

@Table(name = "note")
public class NoteTO extends Entity {

	private static final long serialVersionUID = 1L;

	@Column(name = "beer_id")
	private Long beerId;

	@Column(name = "user_app_id")
	private Long userId;

	@Column(name = "note_desc")
	private String description;
	
	public NoteTO(String description) {
		super();
		this.description = description;
	}

	public Long getBeerId() {
		return beerId;
	}

	public void setBeerId(Long beerId) {
		this.beerId = beerId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
