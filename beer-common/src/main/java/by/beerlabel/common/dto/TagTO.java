package by.beerlabel.common.dto;


import by.beerlabel.common.annotation.Column;
import by.beerlabel.common.annotation.Table;

@Table(name = "tag")
public class TagTO extends Entity {

	private static final long serialVersionUID = 1L;

	@Column(name = "tag_name")
	private String name;
	
	public TagTO(){

	}

	public TagTO(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
