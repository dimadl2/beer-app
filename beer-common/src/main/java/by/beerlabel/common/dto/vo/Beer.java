package by.beerlabel.common.dto.vo;

import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.dto.TagTO;

import java.util.List;



public class Beer {

	private SortTO sort;
	private BeerTO beerTO;
	private Brewery brewery;
	private List<TagTO> tags;

	public Brewery getBrewery() {
		return brewery;
	}

	public void setBrewery(Brewery brewery) {
		this.brewery = brewery;
	}

	public SortTO getSort() {
		return sort;
	}

	public void setSort(SortTO sort) {
		this.sort = sort;
	}

	public BeerTO getBeerTO() {
		return beerTO;
	}

	public void setBeerTO(BeerTO beerTO) {
		this.beerTO = beerTO;
	}

	public List<TagTO> getTags() {
		return tags;
	}

	public void setTags(List<TagTO> tags) {
		this.tags = tags;
	}

}
