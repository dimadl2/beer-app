package by.beerlabel.common.dto.vo;

import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.CountryTO;

/**
 * Created by dima on 14.5.15.
 */
public class Brewery {

    private BreweryTO breweryTO;
    private CountryTO country;

    public CountryTO getCountry() {
        return country;
    }

    public void setCountry(CountryTO country) {
        this.country = country;
    }

    public BreweryTO getBreweryTO() {
        return breweryTO;
    }

    public void setBreweryTO(BreweryTO breweryTO) {
        this.breweryTO = breweryTO;
    }
}
