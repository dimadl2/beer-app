package by.beerlabel.common.service;

import by.beerlabel.common.dto.vo.Beer;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;


public interface BeerManage {
	
	Beer fetchById(Long id) throws TechnicalException, ServiceException;
	List<Beer> getAll() throws TechnicalException, ServiceException;
	List<Beer> searchBeerByName(String name) throws TechnicalException, ServiceException;

	Long save(Beer beer) throws TechnicalException, ServiceException;
}
