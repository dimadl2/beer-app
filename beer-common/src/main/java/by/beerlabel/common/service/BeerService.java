package by.beerlabel.common.service;

import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.util.Filter;

import java.util.List;



public interface BeerService extends Service<BeerTO>{
	
	List<BeerTO> searchBeerByName(String name) throws TechnicalException;
	List<BeerTO> fetchRecomended(String username) throws TechnicalException;

	List<BeerTO> fetchRecentActivity(String userId) throws TechnicalException;

	List<BeerTO> search(Filter filter) throws TechnicalException;
}
