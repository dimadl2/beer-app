package by.beerlabel.common.service;

import by.beerlabel.common.dto.vo.Brewery;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
public interface BreweryManage {

    List<Brewery> getAll() throws TechnicalException, ServiceException;
    Brewery fetchById(Long id) throws TechnicalException, ServiceException;
}
