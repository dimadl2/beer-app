package by.beerlabel.common.service;

import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
public interface BreweryService extends Service<BreweryTO>{
    List<BreweryTO> fetchBreweryByCountry(Long countryId) throws TechnicalException;
}
