package by.beerlabel.common.service;

import by.beerlabel.common.dto.MarkTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;

import java.util.jar.Manifest;

/**
 * Created by dima on 2.6.15.
 */
public interface MarkService extends Service<MarkTO> {


    void setMark(Long beerId, Long userId, Long markId) throws TechnicalException, ServiceException, DAOException;
}
