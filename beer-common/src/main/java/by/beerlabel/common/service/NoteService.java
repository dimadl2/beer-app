package by.beerlabel.common.service;

import by.beerlabel.common.dto.NoteTO;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;



public interface NoteService extends Service<NoteTO> {

	List<NoteTO> fetchNotes(Long beerId, String username) throws TechnicalException;
}
