package by.beerlabel.common.service;

import by.beerlabel.common.dto.Entity;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;



public interface Service<T extends Entity> {
	
	Long save(T object) throws TechnicalException, ServiceException;
	void delete(Long id);
	void delete(List<Long> listId) throws TechnicalException;
	void update(T object) throws TechnicalException;
	List<T> getAll() throws TechnicalException;
	T fetchById(Long id) throws TechnicalException, ServiceException;


}
