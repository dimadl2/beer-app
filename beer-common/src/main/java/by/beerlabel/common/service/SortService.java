package by.beerlabel.common.service;


import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;

public interface SortService extends Service<SortTO> {

    List<SortTO> fetchSortsByCountry(Long countryId) throws TechnicalException;

    List<SortTO> fetchSortsByBrewery(Long breweryId) throws TechnicalException;
}
