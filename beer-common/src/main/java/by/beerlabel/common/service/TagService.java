package by.beerlabel.common.service;

import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.exception.TechnicalException;

import java.util.List;



public interface TagService extends Service<TagTO> {
	
	List<TagTO> fetchTagsByBeerId(Long id) throws TechnicalException;

	void attachTagTOBeer(Long beer, Long tag) throws TechnicalException;
}
