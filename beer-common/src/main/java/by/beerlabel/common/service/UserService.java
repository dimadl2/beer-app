package by.beerlabel.common.service;

import by.beerlabel.common.dto.UserTO;
import by.beerlabel.common.exception.ServiceException;

import javax.sql.rowset.serial.SerialException;



public interface UserService extends Service<UserTO> {
	
	Long fetchIdByUsername(String username) throws ServiceException;

}
