package by.beerlabel.common.service.impl;

import java.util.LinkedList;
import java.util.List;

import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.dto.vo.Brewery;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.dto.vo.Beer;
import by.beerlabel.common.exception.TechnicalException;

@Service
public class BeerManageImpl implements BeerManage {

	
	@Autowired
	private BeerService beerService;

	@Autowired
	private BreweryManage breweryManage;

	@Autowired
	private SortService sortService;
	
	@Autowired
	private TagService tagService;
	
	@Override
	public Beer fetchById(Long id) throws TechnicalException, ServiceException {
		
		BeerTO beerTO = beerService.fetchById(id);
		
		Beer beer = new Beer();
		
		List<TagTO> tags = tagService.fetchTagsByBeerId(id);
		Brewery brewery = breweryManage.fetchById(beerTO.getBreweryId());
		SortTO sort = sortService.fetchById(beerTO.getSortId());

		beer.setBeerTO(beerTO);
		beer.setTags(tags);
		beer.setSort(sort);
		beer.setBrewery(brewery);
		
		return beer;
	}

	@Override
	public List<Beer> getAll() throws TechnicalException, ServiceException {

		List<BeerTO> beerTOs = beerService.getAll();
		List<Beer> beers = new LinkedList<>();
		for (BeerTO beerTO : beerTOs) {

			Brewery brewery = breweryManage.fetchById(beerTO.getBreweryId());
			SortTO sortTO = sortService.fetchById(beerTO.getSortId());
			Beer beer = new Beer();
			beer.setBeerTO(beerTO);
			beer.setBrewery(brewery);
			beer.setSort(sortTO);
			beers.add(beer);

		}

		return beers;

	}

	@Override
	public List<Beer> searchBeerByName(String name) throws TechnicalException, ServiceException {

		List<BeerTO> beerTOs = beerService.searchBeerByName(name);
		List<Beer> beers = new LinkedList<>();
		for (BeerTO beerTO : beerTOs) {

			Brewery brewery = breweryManage.fetchById(beerTO.getBreweryId());
			SortTO sortTO = sortService.fetchById(beerTO.getSortId());
			Beer beer = new Beer();
			beer.setBeerTO(beerTO);
			beer.setBrewery(brewery);
			beer.setSort(sortTO);
			beers.add(beer);

		}

		return beers;

	}

	@Override
	public Long save(Beer beer) throws TechnicalException, ServiceException {

		Long id = beerService.save(beer.getBeerTO());

		for (TagTO tag: beer.getTags()){

			tagService.attachTagTOBeer(id, tag.getId());

		}

		return id;

	}


}
