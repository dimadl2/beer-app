package by.beerlabel.common.service.impl;

import java.util.List;

import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.util.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dao.impl.BeerDAO;
import by.beerlabel.common.dto.BeerTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BeerService;

@Service
public class BeerServiceImpl implements BeerService{
	
	@Autowired
	private BeerDAO beerDAO;

	@Override
	public Long save(BeerTO object) throws ServiceException {

		try {
			return beerDAO.add(object);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(List<Long> listId) throws TechnicalException {
		try {
			beerDAO.delete(listId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void update(BeerTO object) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<BeerTO> getAll() throws TechnicalException {

		try {
			return beerDAO.list();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public BeerTO fetchById(Long id) throws TechnicalException {
		BeerTO beer = null;
		
		try {
			 beer = beerDAO.fetchById(id);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
		
		return beer;
		
	}

	@Override
	public List<BeerTO> searchBeerByName(String name) throws TechnicalException {
		
		List<BeerTO> beers;
		
		try {
			beers = beerDAO.searchBeerByName(name);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}		
		
		return beers;
	}

	@Override
	public List<BeerTO> fetchRecomended(String username) throws TechnicalException {
		
		List<BeerTO> beers = null;
		
		try {
			beers = beerDAO.fetchRecomendedBeer(username);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
		
		return beers;
	}

	@Override
	public List<BeerTO> fetchRecentActivity(String username) throws TechnicalException {

		List<BeerTO> beers = null;

		try {
			beers = beerDAO.fetchRecenActivity(username);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return beers;
	}

	@Override
	public List<BeerTO> search(Filter filter) throws TechnicalException {

		List<BeerTO> beers = null;

		try {
			beers = beerDAO.search(filter);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

		return beers;
	}




}
