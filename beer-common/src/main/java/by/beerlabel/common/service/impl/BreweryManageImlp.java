package by.beerlabel.common.service.impl;

import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.dto.CountryTO;
import by.beerlabel.common.dto.vo.Brewery;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BreweryManage;
import by.beerlabel.common.service.BreweryService;
import by.beerlabel.common.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Service
public class BreweryManageImlp implements BreweryManage {

    @Autowired
    private BreweryService breweryService;
    @Autowired
    private CountryService countryService;

    @Override
    public List<Brewery> getAll() throws TechnicalException, ServiceException {

        List<BreweryTO> breweryTOs = breweryService.getAll();

        List<Brewery> breweries = new LinkedList<>();

        for (BreweryTO breweryTO : breweryTOs) {
            Brewery brewery = new Brewery();
            CountryTO countryTO = countryService.fetchById(breweryTO.getCountryId());
            brewery.setCountry(countryTO);
            brewery.setBreweryTO(breweryTO);
            breweries.add(brewery);

        }
        return breweries;
    }

    @Override
    public Brewery fetchById(Long id) throws TechnicalException, ServiceException {
        BreweryTO breweryTO = breweryService.fetchById(id);
        CountryTO countryTO = countryService.fetchById(breweryTO.getCountryId());
        Brewery brewery = new Brewery();
        brewery.setBreweryTO(breweryTO);
        brewery.setCountry(countryTO);

        return brewery;
    }

}
