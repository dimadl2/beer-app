package by.beerlabel.common.service.impl;

import by.beerlabel.common.dao.impl.BreweryDAO;
import by.beerlabel.common.dto.BreweryTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dima on 14.5.15.
 */
@Service
public class BreweryServiceImpl implements BreweryService{


    @Autowired
    private BreweryDAO breweryDAO;

    @Override
    public Long save(BreweryTO object) throws TechnicalException {

        try {
            return breweryDAO.add(object);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void delete(List<Long> listId) throws TechnicalException {
        try {
            breweryDAO.delete(listId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public void update(BreweryTO object) throws TechnicalException {

        try {
            breweryDAO.update(object);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }

    }

    @Override
    public List<BreweryTO> getAll() throws TechnicalException {

        try {
            return breweryDAO.list();
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public BreweryTO fetchById(Long id) throws TechnicalException {
        try {
            return breweryDAO.fetchById(id);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }

    @Override
    public List<BreweryTO> fetchBreweryByCountry(Long countryId) throws TechnicalException {

        try {
            return breweryDAO.fetchBreweryByCountry(countryId);
        } catch (DAOException e) {
            throw new TechnicalException(e);
        }
    }


}
