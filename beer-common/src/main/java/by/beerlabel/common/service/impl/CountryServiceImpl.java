package by.beerlabel.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dao.impl.CountryDAO;
import by.beerlabel.common.dto.CountryTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.CountryService;

@Service
public class CountryServiceImpl implements CountryService {

	@Autowired
	private CountryDAO countryDAO;
	
	@Override
	public Long save(CountryTO object) throws TechnicalException {
		try {
			return countryDAO.add(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(List<Long> listId) throws TechnicalException {
		try {
			countryDAO.delete(listId);
		} catch (DAOException e) {
			throw new TechnicalException(e);

		}
	}

	@Override
	public void update(CountryTO object) throws TechnicalException {

		try {
			countryDAO.update(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public List<CountryTO> getAll() throws TechnicalException {
		
		try {
			return countryDAO.list();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public CountryTO fetchById(Long id) throws TechnicalException {
		try {
			return countryDAO.fetchById(id);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

}
