package by.beerlabel.common.service.impl;

import by.beerlabel.common.dao.impl.MarkDAO;
import by.beerlabel.common.dto.MarkTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.MarkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by dima on 2.6.15.
 */
@Service
public class MarkServiceImpl implements MarkService {

    @Autowired
    private MarkDAO markDAO;

    @Override
    public Long save(MarkTO object) throws TechnicalException, ServiceException {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void delete(List<Long> listId) throws TechnicalException {

    }

    @Override
    public void update(MarkTO object) throws TechnicalException {

    }

    @Override
    public List<MarkTO> getAll() throws TechnicalException {
        return null;
    }

    @Override
    public MarkTO fetchById(Long id) throws TechnicalException, ServiceException {
        return null;
    }

    @Override
    public void setMark(Long beerId, Long userId, Long markId) throws TechnicalException, ServiceException, DAOException {
        markDAO.setMark(beerId, userId, markId);
    }


}
