package by.beerlabel.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dao.impl.NoteDAO;
import by.beerlabel.common.dto.NoteTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.NoteService;

@Service
public class NoteServiceImpl implements NoteService {

	
	@Autowired
	NoteDAO noteDAO;
	
	@Override
	public Long save(NoteTO object) throws TechnicalException {


		try {
			return noteDAO.add(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(List<Long> listId) throws TechnicalException {
		try {
			noteDAO.delete(listId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public void update(NoteTO object) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<NoteTO> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NoteTO fetchById(Long id) throws TechnicalException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<NoteTO> fetchNotes(Long beerId, String username) throws TechnicalException {
		
		List<NoteTO> notes = null;
		
		try {
			notes = noteDAO.fetchNote(beerId, username);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
		
		return notes;
		
	}

}
