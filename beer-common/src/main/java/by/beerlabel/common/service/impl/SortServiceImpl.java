package by.beerlabel.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dao.impl.SortDAO;
import by.beerlabel.common.dto.SortTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.SortService;

@Service
public class SortServiceImpl implements SortService {

	@Autowired
	private SortDAO sortDAO;
	
	@Override
	public Long save(SortTO object) throws TechnicalException {
		try {
			return sortDAO.add(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(SortTO object) throws TechnicalException {

		try {
			sortDAO.update(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public List<SortTO> getAll() throws TechnicalException {
		
		try {
			return sortDAO.list();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public SortTO fetchById(Long id) throws TechnicalException {

		try {
			return sortDAO.fetchById(id);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void delete(List<Long> sortIds) throws TechnicalException {
		try {
			sortDAO.delete(sortIds);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public List<SortTO> fetchSortsByCountry(Long countryId) throws TechnicalException {

		try {
			return sortDAO.fetchSortsByCountry(countryId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public List<SortTO> fetchSortsByBrewery(Long breweryId) throws TechnicalException {

		try {
			return sortDAO.fetchSortsByCountry(breweryId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}
}
