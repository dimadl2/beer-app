package by.beerlabel.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dao.impl.TagDAO;
import by.beerlabel.common.dto.TagTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.TechnicalException;
import by.beerlabel.common.service.TagService;

@Service
public class TagServiceImpl implements TagService {

	
	@Autowired
	TagDAO tagDAO;
	
	@Override
	public Long save(TagTO object) throws TechnicalException {
		try {
			return tagDAO.add(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(List<Long> listId) throws TechnicalException {
		try {
			tagDAO.delete(listId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void update(TagTO object) throws TechnicalException {
		try {
			tagDAO.update(object);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

	@Override
	public List<TagTO> getAll() throws TechnicalException {

		try {
			return tagDAO.list();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public TagTO fetchById(Long id) throws TechnicalException {

		try {
			return tagDAO.fetchById(id);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public List<TagTO> fetchTagsByBeerId(Long id) throws TechnicalException {
		
		List<TagTO> tags = null;
		
		try {
			tags = tagDAO.fetchTagsByNewsId(id);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
		
		return tags;
	}

	@Override
	public void attachTagTOBeer(Long beer, Long tag) throws TechnicalException {


		try {
			tagDAO.attachTagToBeer(beer, tag);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}

	}

}
