package by.beerlabel.common.service.impl;

import java.util.List;

import by.beerlabel.common.exception.TechnicalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import by.beerlabel.common.dao.impl.UserDAO;
import by.beerlabel.common.dto.UserTO;
import by.beerlabel.common.exception.DAOException;
import by.beerlabel.common.exception.ServiceException;
import by.beerlabel.common.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDAO;

	@Override
	public Long save(UserTO object) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(List<Long> listId) throws TechnicalException {
		try {
			userDAO.delete(listId);
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public void update(UserTO object) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<UserTO> getAll() throws TechnicalException {

		try {
			return userDAO.list();
		} catch (DAOException e) {
			throw new TechnicalException(e);
		}
	}

	@Override
	public UserTO fetchById(Long id) throws ServiceException {

		try {
			return userDAO.fetchById(id);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public Long fetchIdByUsername(String username)
			throws ServiceException {

		
		try {
			return  userDAO.fetchIdByUsername(username);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

	}

}
