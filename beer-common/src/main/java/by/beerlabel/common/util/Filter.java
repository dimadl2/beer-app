package by.beerlabel.common.util;

import java.util.List;

/**
 * Created by dima on 1.6.15.
 */
public class Filter {

    private Long countryId;
    private Long breweryId;
    private Long sortId;
    private String alco;
    private List<Long> tagsId;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(Long breweryId) {
        this.breweryId = breweryId;
    }

    public Long getSortId() {
        return sortId;
    }

    public void setSortId(Long sortId) {
        this.sortId = sortId;
    }

    public String getAlco() {
        return alco;
    }

    public void setAlco(String alco) {
        this.alco = alco;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }
}
