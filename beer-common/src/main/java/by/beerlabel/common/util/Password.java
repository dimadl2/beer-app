package by.beerlabel.common.util;

public class Password {

	private final static String SYMBOLS = "qwertyuiopasdfghjklzxcvbnm1234567890";

	private static final int SIZE_SALT = 5;

	private static String generateSalt() {

		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < SIZE_SALT; i++) {

			builder.append(SYMBOLS.charAt((int) (Math.random() * SYMBOLS
					.length())));

		}

		return builder.toString();

	}

	public static String hash(String password) {

		StringBuilder builder = new StringBuilder();
		builder.append(generateSalt());
		builder.append(password.hashCode());

		return builder.toString();

	}

	public static boolean verifyPass(String password, String verPassword) {

		
		String verHash = hash(verPassword);
		
		return password.substring(SIZE_SALT).equals(
				verHash.substring(SIZE_SALT));

	}

}
